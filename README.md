# shell-backend

A backend for the SIE shell.
Cf. https://gitea.gfa-demo.de/sie/META/issues/3

## Use

The program performs authentication against LDAP using http basic.
Ldap-configurations can be set up in config/ldap-config.json.

Verified users can make requests to get data for the shell's interface. Admin users
are allowed to request changes (add more data).
Initial data is set in data/demoData.json, which may be changed as desired.
The file is imported in data-module.ts and router-module.ts, so when demoData.json's
name is changed, these scripts need to be adjusted accordingly.
