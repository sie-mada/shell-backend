declare module "crypt3/q" {
    function q(key: string, salt?: string) : Promise<string>;

    namespace q {
        function createSalt(type?: 'md5' | 'blowfish' | 'sha256' | 'sha512') : string;
    }

    export = q;
}

declare module "crypt3/async" {
    function async(key: string, salt: string, callback: (err: Error | undefined, value: string) => void) : void;
    function async(key: string, callback: (err: Error | undefined, value: string) => void) : void;

    namespace async {
        function createSalt(type?: 'md5' | 'blowfish' | 'sha256' | 'sha512') : string;
    }

    export = async;
}

declare module "@idango/crypt3/sync" {
    function sync(key: string, salt?: string) : string;

    namespace sync {
        function createSalt(type?: 'md5' | 'blowfish' | 'sha256' | 'sha512') : string;
    }

    export = sync;
}
