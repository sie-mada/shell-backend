export const INJECTABLES = {
    App: Symbol('App'),
    Auth: Symbol('Auth'),
    Config: Symbol('Config'),
    Controller: Symbol('Controller'),
    Dashboard: Symbol('Dashboard'),
    DashboardAdapter: Symbol('DashboardAdapter'),
    Data: Symbol('Data'),
    DataAdapter: Symbol('DataAdapter'),
    DataImport: Symbol('DataImport'),
    JoomlaFixer: Symbol('JoomlaFixer'),
    Ldap: Symbol('Ldap'),
    Router: Symbol('Router')
};
