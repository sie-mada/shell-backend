import { Container } from 'inversify';
// tslint:disable-next-line: no-import-side-effect
import 'reflect-metadata';
import { App, Controller, RouterModule, RouterService, SieBackendApp } from './app';
import { AuthService, AuthServiceImpl, LdapService, LdapServiceImpl, UserController } from './auth';
import { ConfigService, ConfigServiceImpl } from './config';
import { CartoDbDashboardAdapter, DashboardController, DashboardService, DashboardServiceImpl, RemoteDashboardAdapter, SupersetDashboardAdapter } from './dashboards';
import { DataAdapter, DataImportController, DataImportService, DataImportServiceImpl, DataService, DataServiceImpl, PouchDbDataAdapter } from './data';
import { INJECTABLES } from './injectables';
import { JoomlaFixerController } from './tasks/fix-joomla/joomla-fixer-controller';
import { JoomlaFixerService } from './tasks/fix-joomla/joomla-fixer-service';
import { JoomlaFixerServiceImpl } from './tasks/fix-joomla/joomla-fixer-service-impl';

const container = new Container();

// tslint:disable: newline-per-chained-call
container.bind<App>(INJECTABLES.App).to(SieBackendApp).inSingletonScope();
container.bind<ConfigService>(INJECTABLES.Config).to(ConfigServiceImpl).inSingletonScope();
container.bind<LdapService>(INJECTABLES.Ldap).to(LdapServiceImpl).inSingletonScope();
container.bind<AuthService>(INJECTABLES.Auth).to(AuthServiceImpl).inSingletonScope();
container.bind<Controller>(INJECTABLES.Controller).to(UserController).inSingletonScope();
container.bind<DataService>(INJECTABLES.Data).to(DataServiceImpl).inSingletonScope();
container.bind<DataAdapter>(INJECTABLES.DataAdapter).to(PouchDbDataAdapter).inSingletonScope();
container.bind<DashboardService>(INJECTABLES.Dashboard).to(DashboardServiceImpl).inSingletonScope();
container.bind<RemoteDashboardAdapter>(INJECTABLES.DashboardAdapter).to(CartoDbDashboardAdapter).inSingletonScope();
container.bind<RemoteDashboardAdapter>(INJECTABLES.DashboardAdapter).to(SupersetDashboardAdapter).inSingletonScope();
container.bind<Controller>(INJECTABLES.Controller).to(DashboardController).inSingletonScope();
container.bind<RouterService>(INJECTABLES.Router).to(RouterModule).inSingletonScope();
container.bind<Controller>(INJECTABLES.Controller).to(DataImportController).inSingletonScope();
container.bind<DataImportService>(INJECTABLES.DataImport).to(DataImportServiceImpl).inSingletonScope();
container.bind<JoomlaFixerService>(INJECTABLES.JoomlaFixer).to(JoomlaFixerServiceImpl).inSingletonScope();
container.bind<Controller>(INJECTABLES.Controller).to(JoomlaFixerController).inSingletonScope();
// tslint:enable: newline-per-chained-call

export { container };
