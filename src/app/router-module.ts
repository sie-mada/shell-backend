import * as Router from '@koa/router';
import { NOT_FOUND } from 'http-status-codes';
import { injectable, multiInject } from 'inversify';
import { INJECTABLES } from '../injectables';
import { Controller, RouterService } from './types';

@injectable()
export class RouterModule implements RouterService {

    constructor(@multiInject(INJECTABLES.Controller) controllers: Controller[]) {
        controllers.forEach(controller => controller.registerRoutes(this.router));
    }

    private readonly router = new Router()
        .get('/', ctx => ctx.body = 'SIE MG 1.0.1')
        .get('/favicon.ico', ctx => ctx.status = NOT_FOUND);

    get routes() {
        return this.router.routes();
    }
}
