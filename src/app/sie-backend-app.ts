import * as Cors from '@koa/cors';
import * as Router from '@koa/router';
import { inject, injectable } from 'inversify';
import * as Koa from 'koa';
import * as koaBody from 'koa-body';
import { Middleware } from 'koa-compose';
import { AuthService } from '../auth';
import { ConfigService } from '../config';
import { INJECTABLES } from '../injectables';
import { App } from './types';
import { logger, loggingService } from '../util/logger-service'; 

@injectable()
export class SieBackendApp implements App {

    private readonly app: Koa;
    private readonly config: any;

    constructor(
        @inject(INJECTABLES.Auth) private readonly authService: AuthService,
        @inject(INJECTABLES.Config) configService: ConfigService
    ) {
        this.config = configService.getSection('base');
        this.app = this.build();
    }

    private build() {
        const app = new Koa();
        app.use(loggingService(logger));
        app.use(Cors({
            allowHeaders: ['Authorization', 'Content-Type'],
            allowMethods: ['PATCH', 'POST', 'PUT', 'GET', 'DELETE'],
            credentials: true
        }));
        this.authService.addBasicAuth(app);
        app.use(koaBody({
            formLimit: '64M',
            multipart: true
        }));

        return app;
    }

    start(routes: Middleware<Koa.ParameterizedContext<any, Router.RouterContext>>) {
        this.app.use(routes);
        this.app.listen(this.config.port);

        console.log(`Server running on port ${this.config.port}`);
    }
}
