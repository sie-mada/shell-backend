import * as Knex from 'knex';
import { Dashboard, Section } from '../dashboards/types';
import { StoredDashboardComment, StoredDashConfiguration, StoredSectionConfiguration } from './types';

export type DataItem<CONTENT extends { }> = { _id: string; } & CONTENT;

export interface DataAdapter {
    getSections() : Promise<DataItem<StoredSectionConfiguration>[]>;
    getDashboards() : Promise<DataItem<StoredDashConfiguration>[]>;
    setDashboardConfig(dashboard: Partial<DataItem<StoredDashConfiguration>>) : Promise<DataItem<StoredDashConfiguration>>;
    setSectionConfig(section: Partial<DataItem<StoredSectionConfiguration>>) : Promise<DataItem<StoredSectionConfiguration>>;
    deleteSection(id: string) : Promise<void>;
    getComments(adapter: string, remoteId: string) : Promise<DataItem<StoredDashboardComment>[]>;
    addComment(adapter: string, remoteId: string, comment: StoredDashboardComment) : Promise<DataItem<StoredDashboardComment>>;
}

export interface DataService {
    readonly warehouse: Knex;

    getSections() : Promise<DataItem<StoredSectionConfiguration>[]>;
    getDashboards() : Promise<DataItem<StoredDashConfiguration>[]>;
    setDashboardConfig(dashboard: Dashboard) : Promise<DataItem<StoredDashConfiguration>>;
    setSectionConfig(section: Section) : Promise<DataItem<StoredSectionConfiguration>>;
    deleteSection(section: Section) : Promise<void>;
    getComments(adapter: string, remoteId: string) : Promise<DataItem<StoredDashboardComment>[]>;
    addComment(adapter: string, remoteId: string, comment: StoredDashboardComment) : Promise<DataItem<StoredDashboardComment>>;
}
