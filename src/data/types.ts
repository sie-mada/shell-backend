
export interface StoredDashConfiguration {
    adapter: string;
    remoteId: string | number;
    url: string;
    title: string;
    assignedTo: string;
}

export interface StoredSectionConfiguration {
    name: string;
    title: string;
    parent?: string;
    weight?: number;
}

export interface StoredDashboardComment {
    adapter: string;
    remoteId: string;
    user: string;
    timestamp: string;
    content: string;
}
