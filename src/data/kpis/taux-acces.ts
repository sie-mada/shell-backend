import { logger } from '../../util/logger-service';
import { DataService } from '../data-service';
import { KpiList, KpiElement } from './types';

const TAUX_ACCESS_OLD_STRING = `SELECT DATE_TRUNC('month', date) AS __timestamp,
    SUM(desservi)/SUM(population) AS "TauxDAcces"
    FROM warehouse.region_details
    WHERE date >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
    GROUP BY DATE_TRUNC('month', date)
    ORDER BY __timestamp ASC`;

const TAUX_ACCESS_OLD_STRING_SHORT = `SELECT DATE_TRUNC('month', date) AS __timestamp,
    SUM(desservi)/SUM(population) AS "TauxDAcces"
    FROM warehouse.region_details
    WHERE date >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
    GROUP BY DATE_TRUNC('month', date)
    ORDER BY __timestamp ASC
    LIMIT 100`;

const TAUX_ACCESS_NEW_STRING =`SELECT DATE_TRUNC('month', date) AS __timestamp,
    SUM(desservi)/SUM(population) AS "TauxDAcces"
    FROM warehouse.region_details_copy
    WHERE date >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
    GROUP BY DATE_TRUNC('month', date)
    ORDER BY __timestamp ASC`;

const TAUX_ACCESS_NEW_STRING_SHORT =`SELECT DATE_TRUNC('month', date) AS __timestamp,
    SUM(desservi)/SUM(population) AS "TauxDAcces"
    FROM warehouse.region_details_copy
    WHERE date >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
    GROUP BY DATE_TRUNC('month', date)
    ORDER BY __timestamp ASC
    LIMIT 100`;

export async function getTauxDAccesOldValues ( 
    dataService: DataService, 
    kpiElement: KpiElement, 
    shortQuery: boolean) {

    logger.debug('KpisImpl:getTauxDAccesOldValues');
    await dataService.warehouse
        .raw(shortQuery? TAUX_ACCESS_OLD_STRING_SHORT : TAUX_ACCESS_OLD_STRING)
        .then(entries => {
            const dataRows = entries.rows;
            for (let item of dataRows) {
                kpiElement.oldValues.push(item.TauxDAcces);
                kpiElement.oldLabels.push(
                    new Date(item.__timestamp)
                        .toISOString()
                        .slice(0, 10)
                );
            }
        })
        .catch(e => {
            logger.error(`Error getTauxDAccesOldValues: ${e}`);
        });
    logger.debug('KpisImpl:getTauxDAccesOldValues done');
}

export async function getTauxDAccesNewValues (
    dataService: DataService,
    kpiElement: KpiElement, 
    shortQuery: boolean) {
    
    logger.debug('KpisImpl:getTauxDAccesNewValues');
    await dataService.warehouse
        .raw(shortQuery? TAUX_ACCESS_NEW_STRING_SHORT : TAUX_ACCESS_NEW_STRING)
        .then(entries => {
            const dataRows = entries.rows;
            for (let item of dataRows) {
                kpiElement.newValues.push(item.TauxDAcces);
                kpiElement.newLabels.push(
                    new Date(item.__timestamp)
                        .toISOString()
                        .slice(0, 10)
                );
            }
        })
        .catch(e => {
            logger.error(`Error getTauxDAccesNewValues: ${e}`);
        });
    logger.debug('KpisImpl:getTauxDAccesNewValues done');
}

