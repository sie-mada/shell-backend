import { logger } from '../../util/logger-service';
import { DataService } from '../data-service';
import { KpiElement } from './types';


export async function getPuissanceDisponibleNewValues(dataService: DataService, kpiElement: KpiElement) {

    logger.debug('KpisImpl:getPuissanceDisponibleNewValues');

    await dataService.warehouse
        .raw(`SELECT DATE_TRUNC('month', mois) AS __timestamp,
                SUM(valeur)/1000 AS "PuissanceDisponible"
            FROM warehouse.matrice_complete_copy
            WHERE mois >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
            AND categorie = 'power_installed'
            GROUP BY DATE_TRUNC('month', mois)
            ORDER BY __timestamp ASC;`
        )
        .then(entries => {
            const dataRows = entries.rows;
            for (let item of dataRows) {
                kpiElement.newValues.push(item.PuissanceDisponible);
                kpiElement.newLabels.push(
                    new Date(item.__timestamp)
                        .toISOString()
                        .slice(0, 10)
                );
            }
        })
        .catch(e => {
            logger.error(`Error getPuissanceDisponibleNewValues: ${e}`);
        });

    logger.debug('KpisImpl:getPuissanceDisponibleNewValues done');
}

export async function getPuissanceDisponibleOldValues(dataService: DataService, kpiElement: KpiElement) {

    logger.debug('KpisImpl:getPuissanceDisponibleOldValues');

    await dataService.warehouse
        .raw(`SELECT DATE_TRUNC('month', mois) AS __timestamp,
                SUM(valeur)/1000 AS "PuissanceDisponible"
            FROM warehouse.matrice_complete
            WHERE mois >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
            AND categorie = 'power_installed'
            GROUP BY DATE_TRUNC('month', mois)
            ORDER BY __timestamp ASC;`
        )
        .then(entries => {
            const dataRows = entries.rows;
            for (let item of dataRows) {
                kpiElement.oldValues.push(item.PuissanceDisponible);
                kpiElement.oldLabels.push(
                    new Date(item.__timestamp)
                        .toISOString()
                        .slice(0, 10)
                );
            }
        })
        .catch(e => {
            logger.error(`Error getPuissanceDisponibleOldValues: ${e}`);
        });

    logger.debug('KpisImpl:getPuissanceDisponibleOldValues done');
}
