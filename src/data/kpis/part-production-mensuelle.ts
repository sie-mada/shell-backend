import { logger } from '../../util/logger-service';
import { DataService } from '../data-service';
import { KpiElement } from './types';

const PART_PROD_MENSUELLE_NEW_STRING = 
    `SELECT SUM( case renouvelable
           when True then valeur
           else 0
        end)/sum(valeur) AS "partRenewable",
        DATE_TRUNC('month', mois) AS __timestamp
    FROM warehouse.matrice_complete_copy
    WHERE mois >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
    AND categorie = 'production'
    AND type_energie IS NOT NULL
    GROUP BY DATE_TRUNC('month', mois)
    ORDER BY __timestamp ASC`;

const PART_PROD_MENSUELLE_OLD_STRING = 
    `SELECT SUM( case renouvelable
           when True then valeur
           else 0
        end)/sum(valeur) AS "partRenewable",
        DATE_TRUNC('month', mois) AS __timestamp
    FROM warehouse.matrice_complete
    WHERE mois >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
    AND categorie = 'production'
    AND type_energie IS NOT NULL
    GROUP BY DATE_TRUNC('month', mois)
    ORDER BY __timestamp ASC`;

export async function getPartProdMensuelleNewValues(dataService: DataService, kpiElement: KpiElement) {

    logger.debug('KpisImpl:getPartProdMensuelleNewValues');

    await dataService.warehouse
        .raw(PART_PROD_MENSUELLE_NEW_STRING)
        .then(entries => {
            const dataRows = entries.rows;
            for (let item of dataRows) {
                kpiElement.newValues.push(item.partRenewable);
                kpiElement.newLabels.push(
                    new Date(item.__timestamp)
                        .toISOString()
                        .slice(0, 10)
                );
            }
        })
        .catch(e => {
            logger.error(`Error getPartProdMensuelleNewValues: ${e}`);
        });

    logger.debug('KpisImpl:getPartProdMensuelleNewValues done');
}

export async function getPartProdMensuelleOldValues(dataService: DataService, kpiElement: KpiElement) {

    logger.debug('KpisImpl:getPartProdMensuelleOldValues');

    await dataService.warehouse
        .raw(PART_PROD_MENSUELLE_OLD_STRING
        )
        .then(entries => {
            const dataRows = entries.rows;
            for (let item of dataRows) {
                kpiElement.oldValues.push(item.partRenewable);
                kpiElement.oldLabels.push(
                    new Date(item.__timestamp)
                        .toISOString()
                        .slice(0, 10)
                );
            }
        })
        .catch(e => {
            logger.error(`Error getPartProdMensuelleOldValues: ${e}`);
        });

    logger.debug('KpisImpl:getPartProdMensuelleOldValues done');
}
