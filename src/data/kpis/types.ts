export type KpiList = KpiElement [];

export interface KpiElementInterface  {
    name : string;
    oldValues: number[];
    newValues: number[];
    oldLabels: string[];
    newLabels: string[];
}

export interface KpiConstructor {
    new (name: string) : KpiElement;
}

export function createKpi (ctor: KpiConstructor, name: string) : KpiElement {

    return new ctor(name);
}

export class KpiElement implements KpiElementInterface {

    readonly name : string;
    readonly oldValues: number[];
    readonly newValues: number[];
    readonly oldLabels: string[];
    readonly newLabels: string[];

    constructor(kpiName: string) { 
        this.name = kpiName;
        this.oldValues = [];
        this.oldLabels = [];
        this.newValues = [];
        this.newLabels = [];
    }
}
