import { logger } from '../../util/logger-service';
import { DataService } from '../data-service';
import { KpiList, KpiElement, createKpi } from './types';
import { readFileSync, writeFile } from 'fs';
import { getConnexionsTotalesOldValues, getConnexionsTotalesNewValues } from './connexions-totales';
import { getNouvelleConnexionsNewValues, getNouvelleConnexionsOldValues } from './nouvelle-connexions';
import { getPartProdMensuelleOldValues, getPartProdMensuelleNewValues } from './part-production-mensuelle';
import { getProductionAnnuelleOldValues, getProductionAnnuelleNewValues } from './production-annuelle';
import { getPuissanceInstalleeOldValues, getPuissanceInstalleeNewValues } from './puissance-installee';
import { getPuissanceDisponibleOldValues, getPuissanceDisponibleNewValues } from './puissance-disponible';
import { getPartEnergieSolaireOldValues, getPartEnergieSolaireNewValues,
    getPartEnergieHydroOldValues, getPartEnergieHydroNewValues 
} from './part-energie';
import { getTauxDAccesOldValues, getTauxDAccesNewValues } from './taux-acces';
import { getPopulationDesservieOldValues, getPopulationDesservieNewValues } from './population-desservie';


export async function getKpiValues(importId: string, dataService: DataService) : Promise<KpiList> {

    logger.debug('kpis:getKpiValues');
    let kpiList : KpiList = [];

    const tauDAccessElement = createKpi(KpiElement, 'Taux d\'accès à l\'électricité (%)');
    await getTauxDAccesOldValues(dataService, tauDAccessElement, false);
    await getTauxDAccesNewValues(dataService, tauDAccessElement, false);
    kpiList.push(tauDAccessElement);

    const popDesservie = createKpi(KpiElement, 'Population Desservie');
    await getPopulationDesservieOldValues(dataService, popDesservie);
    await getPopulationDesservieNewValues(dataService, popDesservie);
    kpiList.push(popDesservie);

    const connexionsTotales = createKpi(KpiElement, 'Connexions Totales');
    await getConnexionsTotalesOldValues(dataService, connexionsTotales, false);
    await getConnexionsTotalesNewValues(dataService, connexionsTotales, false);
    kpiList.push(connexionsTotales);

    const nouvelleConnexions = createKpi(KpiElement, 'Nouvelle Connexions');
    await getNouvelleConnexionsOldValues(dataService, nouvelleConnexions, false);
    await getNouvelleConnexionsNewValues(dataService, nouvelleConnexions, false);
    kpiList.push(nouvelleConnexions);

    const productionAnnuelle = createKpi(KpiElement, 'Production Annuelle (MWh)');
    await getProductionAnnuelleOldValues(dataService, productionAnnuelle);
    await getProductionAnnuelleNewValues(dataService, productionAnnuelle);
    kpiList.push(productionAnnuelle);
    
    const puissanceInstallee = createKpi(KpiElement, 'Puissance Installee (MW)');
    await getPuissanceInstalleeOldValues (dataService, puissanceInstallee);
    await getPuissanceInstalleeNewValues(dataService, puissanceInstallee);
    kpiList.push(puissanceInstallee);

    const puissanceDisponible = createKpi(KpiElement, 'Puissance Disponible (MW)');
    await getPuissanceDisponibleOldValues (dataService, puissanceDisponible);
    await getPuissanceDisponibleNewValues(dataService, puissanceDisponible);
    kpiList.push(puissanceDisponible);

    const partProdMensuelle = createKpi(KpiElement, 'Part Production Mensuelle Renouvable (%)');
    await getPartProdMensuelleOldValues (dataService, partProdMensuelle);
    await getPartProdMensuelleNewValues (dataService, partProdMensuelle);
    kpiList.push(partProdMensuelle);

    const partEnergieSolaire = createKpi(KpiElement, 'Part Energie Solaire (%)');
    await getPartEnergieSolaireOldValues (dataService, partEnergieSolaire);
    await getPartEnergieSolaireNewValues (dataService, partEnergieSolaire);
    kpiList.push(partEnergieSolaire);

    const partEnergieHydro = createKpi(KpiElement, 'Part Energie Hydro (%)');
    await getPartEnergieHydroOldValues (dataService, partEnergieHydro);
    await getPartEnergieHydroNewValues (dataService, partEnergieHydro);
    kpiList.push(partEnergieHydro);

    const ret = new Promise<KpiList>(resolve => {
        logger.debug('kpis:getKpiValues resolve');
        resolve(kpiList);
    });

    return ret;
}




export function storeKpis(importId: string, kpiList : KpiList, filePpath: string) {
    logger.debug(`kpis:storeKpis id: ${importId}, filePpath: ${filePpath}`);
    const fileString = JSON.stringify(kpiList);
    writeFile(filePpath, fileString, err  => {
        if (err) {
            logger.error(`Error storeKpis, write file ${filePpath} failed, reason: ${err}`);
            throw err;
        }
        logger.debug('kpis:storeKpis write file done');    
    });
    logger.debug('kpis:storeKpis done');
}

export function loadKpis (importId: string, dataPath: string, kpiList : KpiList) {
    logger.debug(`kpis:loadKpis id: ${importId}, path: ${dataPath}`);
    let rawData = readFileSync(dataPath);
    const structure = JSON.parse(rawData.toString());
    for (const ele of structure) {
        kpiList.push(ele as unknown as KpiElement);
    }
    logger.debug('kpis:loadKpis done');
}
