import { logger } from '../../util/logger-service';
import { DataService } from '../data-service';
import { KpiElement } from './types';

export async function getProductionAnnuelleNewValues(dataService: DataService, kpiElement: KpiElement) {

    logger.debug('kpis:getProductionAnnuelleNewValues');

    await dataService.warehouse
        .raw(`  SELECT DATE_TRUNC('year', mois) AS __timestamp,
                    sum(valeur) AS "ProductionAnnuelle"
                FROM warehouse.matrice_complete_copy
                WHERE mois >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
                AND categorie = 'production'
                AND type_energie IS NOT NULL
                GROUP BY DATE_TRUNC('year', mois)
                ORDER BY __timestamp ASC;`
        )
        .then(entries => {
            const dataRows = entries.rows;
            for (let item of dataRows) {
                kpiElement.newValues.push(item.ProductionAnnuelle);
                kpiElement.newLabels.push(
                    new Date(item.__timestamp)
                        .toISOString()
                        .slice(0, 10)
                );
            }
        })
        .catch(e => {
            logger.error(`Error getProductionAnnuelleNewValues: ${e}`);
        });

    logger.debug('kpis:getProductionAnnuelleNewValues done');
}

export async function getProductionAnnuelleOldValues(dataService: DataService, kpiElement: KpiElement) {

    logger.debug('kpis:getProductionAnnuelleOldValues');

    await dataService.warehouse
        .raw(`SELECT DATE_TRUNC('year', mois) AS __timestamp,
                sum(valeur) AS "ProductionAnnuelle"
            FROM warehouse.matrice_complete
            WHERE mois >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
            AND categorie = 'production'
            AND type_energie IS NOT NULL
            GROUP BY DATE_TRUNC('year', mois)
            ORDER BY __timestamp ASC;`
        )
        .then(entries => {
            const dataRows = entries.rows;
            for (let item of dataRows) {
                kpiElement.oldValues.push(item.ProductionAnnuelle);
                kpiElement.oldLabels.push(
                    new Date(item.__timestamp)
                        .toISOString()
                        .slice(0, 10)
                );
            }
        })
        .catch(e => {
            logger.error(`Error getProductionAnnuelleOldValues: ${e}`);
        });

    logger.debug('kpis:getProductionAnnuelleOldValues done');
}
