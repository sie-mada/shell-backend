import { logger } from '../../util/logger-service';
import { DataService } from '../data-service';
import { KpiList, KpiElement } from './types';

export async function getPopulationDesservieOldValues(dataService: DataService, kpiElement: KpiElement) {

    logger.debug('KpisImpl:getPopulationDesservie getPopulationDesservieOldValues');

    await dataService.warehouse
        .raw(`SELECT DATE_TRUNC('month', date) AS __timestamp,
            sum(desservi) AS "PopulationDesservie"
            FROM warehouse.region_details
            WHERE date >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
            GROUP BY DATE_TRUNC('month', date)
            ORDER BY __timestamp ASC;`
        )
        .then(entries => {
            const dataRows = entries.rows;
            for (let item of dataRows) {
                kpiElement.oldValues.push(item.PopulationDesservie);
                kpiElement.oldLabels.push(
                    new Date(item.__timestamp)
                        .toISOString()
                        .slice(0, 10)
                );
            }
        })
        .catch(e => {
            logger.error(`Error getPopulationDesservieOldValues: ${e}`);
        });

    logger.debug('KpisImpl:getPopulationDesservieOldValues done');
}

export async function getPopulationDesservieNewValues(dataService: DataService, kpiElement: KpiElement) {

    logger.debug('KpisImpl:getPopulationDesservie getPopulationDesservieNewValues');

    await dataService.warehouse
        .raw(`SELECT DATE_TRUNC('month', date) AS __timestamp,
            sum(desservi) AS "PopulationDesservie"
            FROM warehouse.region_details_copy
            WHERE date >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
            GROUP BY DATE_TRUNC('month', date)
            ORDER BY __timestamp ASC;`
        )
        .then(entries => {
            const dataRows = entries.rows;
            for (let item of dataRows) {
                kpiElement.newValues.push(item.PopulationDesservie);
                kpiElement.newLabels.push(
                    new Date(item.__timestamp)
                        .toISOString()
                        .slice(0, 10)
                );
            }
        })
        .catch(e => {
            logger.error(`Error getPopulationDesservieNewValues: ${e}`);
        });

    logger.debug('KpisImpl:getPopulationDesservieNewValues done');
}

