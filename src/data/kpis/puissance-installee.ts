import { logger } from '../../util/logger-service';
import { DataService } from '../data-service';
import { KpiElement } from './types';


export async function getPuissanceInstalleeNewValues(dataService: DataService, kpiElement: KpiElement) {

    logger.debug('KpisImpl:getPuissanceInstalleeNewValues');

    await dataService.warehouse
        .raw(`SELECT DATE_TRUNC('month', mois) AS __timestamp,
                SUM(valeur)/1000 AS "PuissanceInstallee"
            FROM warehouse.matrice_complete_copy
            WHERE mois >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
            AND categorie = 'power_installed'
            GROUP BY DATE_TRUNC('month', mois)
            ORDER BY __timestamp ASC;`
        )
        .then(entries => {
            const dataRows = entries.rows;
            for (let item of dataRows) {
                kpiElement.newValues.push(item.PuissanceInstallee);
                kpiElement.newLabels.push(
                    new Date(item.__timestamp)
                        .toISOString()
                        .slice(0, 10)
                );
            }
        })
        .catch(e => {
            logger.error(`Error getPuissanceInstalleeNewValues: ${e}`);
        });

    logger.debug('KpisImpl:getPuissanceInstalleeNewValues done');
}

export async function getPuissanceInstalleeOldValues(dataService: DataService, kpiElement: KpiElement) {

    logger.debug('KpisImpl:getPuissanceInstalleeOldValues');

    await dataService.warehouse
        .raw(`SELECT DATE_TRUNC('month', mois) AS __timestamp,
                SUM(valeur)/1000 AS "PuissanceInstallee"
            FROM warehouse.matrice_complete
            WHERE mois >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
            AND categorie = 'power_installed'
            GROUP BY DATE_TRUNC('month', mois)
            ORDER BY __timestamp ASC;`
        )
        .then(entries => {
            const dataRows = entries.rows;
            for (let item of dataRows) {
                kpiElement.oldValues.push(item.PuissanceInstallee);
                kpiElement.oldLabels.push(
                    new Date(item.__timestamp)
                        .toISOString()
                        .slice(0, 10)
                );
            }
        })
        .catch(e => {
            logger.error(`Error getPuissanceInstalleeOldValues: ${e}`);
        });

    logger.debug('KpisImpl:getPuissanceInstalleeOldValues done');
}
