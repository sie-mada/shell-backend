import { logger } from '../../util/logger-service';
import { DataService } from '../data-service';
import { KpiElement } from './types';

export async function getPartEnergieSolaireOldValues(dataService: DataService, kpiElement: KpiElement) {

    logger.debug('KpisImpl:getPartEnergieSolaireOldValues');

    await dataService.warehouse
        .raw(`SELECT DATE_TRUNC('month', mois) AS __timestamp,
                    SUM(case type_energie
                            when 's' then valeur
                            else 0
                        end)/sum(valeur) AS "sumEnergie"
            FROM warehouse.matrice_complete
            WHERE mois >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
            AND categorie = 'production'
            AND type_energie IS NOT NULL
            GROUP BY DATE_TRUNC('month', mois)
            ORDER BY __timestamp ASC;`
        )
        .then(entries => {
            const dataRows = entries.rows;
            for (let item of dataRows) {
                kpiElement.oldValues.push(item.sumEnergie*100);
                kpiElement.oldLabels.push(
                    new Date(item.__timestamp)
                        .toISOString()
                        .slice(0, 10)
                );
            }
        })
        .catch(e => {
            logger.error(`Error getPartEnergieSolaireOldValues: ${e}`);
        });

    logger.debug('KpisImpl:getPartEnergieSolaireOldValues done');
}

export async function getPartEnergieSolaireNewValues(dataService: DataService, kpiElement: KpiElement) {

    logger.debug('KpisImpl:getPartEnergieSolaireNewValues');

    await dataService.warehouse
        .raw(`SELECT DATE_TRUNC('month', mois) AS __timestamp,
                SUM(case type_energie
                        when 's' then valeur
                        else 0
                    end)/sum(valeur) AS "sumEnergie"
            FROM warehouse.matrice_complete_copy
            WHERE mois >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
            AND categorie = 'production'
            AND type_energie IS NOT NULL
            GROUP BY DATE_TRUNC('month', mois)
            ORDER BY __timestamp ASC;`
        )
        .then(entries => {
            const dataRows = entries.rows;
            for (let item of dataRows) {
                kpiElement.newValues.push(item.sumEnergie*100);
                kpiElement.newLabels.push(
                    new Date(item.__timestamp)
                        .toISOString()
                        .slice(0, 10)
                );
            }
        })
        .catch(e => {
            logger.error(`Error getPartEnergieSolaireNewValues: ${e}`);
        });

    logger.debug('KpisImpl:getPartEnergieSolaireNewValues done');
}

export async function getPartEnergieHydroOldValues(dataService: DataService, kpiElement: KpiElement) {

    logger.debug('KpisImpl:getPartEnergieHydroOldValues');

    await dataService.warehouse
        .raw(`SELECT DATE_TRUNC('month', mois) AS __timestamp,
                SUM(case type_energie
                        when 'h' then valeur
                        else 0
                    end)/sum(valeur) AS "sumEnergie"
                FROM warehouse.matrice_complete
                WHERE mois >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
                AND categorie = 'production'
                AND type_energie IS NOT NULL
                GROUP BY DATE_TRUNC('month', mois)
                ORDER BY __timestamp ASC;`
        )
        .then(entries => {
            const dataRows = entries.rows;
            for (let item of dataRows) {
                kpiElement.oldValues.push(item.sumEnergie*100);
                kpiElement.oldLabels.push(
                    new Date(item.__timestamp)
                        .toISOString()
                        .slice(0, 10)
                );
            }
        })
        .catch(e => {
            logger.error(`Error getPartEnergieHydroOldValues: ${e}`);
        });

    logger.debug('KpisImpl:getPartEnergieHydroOldValues done');
}

export async function getPartEnergieHydroNewValues(dataService: DataService, kpiElement: KpiElement) {

    logger.debug('KpisImpl:getPartEnergieHydroNewValues');

    await dataService.warehouse
        .raw(`SELECT DATE_TRUNC('month', mois) AS __timestamp,
                SUM(case type_energie
                        when 'h' then valeur
                        else 0
                    end)/sum(valeur) AS "sumEnergie"
                FROM warehouse.matrice_complete_copy
                WHERE mois >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
                AND categorie = 'production'
                AND type_energie IS NOT NULL
                GROUP BY DATE_TRUNC('month', mois)
                ORDER BY __timestamp ASC;`
        )
        .then(entries => {
            const dataRows = entries.rows;
            for (let item of dataRows) {
                kpiElement.newValues.push(item.sumEnergie*100);
                kpiElement.newLabels.push(
                    new Date(item.__timestamp)
                        .toISOString()
                        .slice(0, 10)
                );
            }
        })
        .catch(e => {
            logger.error(`Error getPartEnergieHydroNewValues: ${e}`);
        });

    logger.debug('KpisImpl:getPartEnergieHydroNewValues done');
}
