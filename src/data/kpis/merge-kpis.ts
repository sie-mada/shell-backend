import { logger } from '../../util/logger-service';
import { KpiElement } from './types';
import * as moment from 'moment';

export function sortDates(axis: string[]) : string[] {
    logger.debug('sortAxis');

    // long form
    // const sorted = axis.slice()
    //     .sort((a : string, b: string) => {
    // let date1 : Date = moment(a)
    // .toDate();
    // let date2 : Date = moment(b)
    // .toDate();
    // return (date1.getTime() - date2.getTime());
    // });

    const sorted = axis.slice()
        .sort((a, b) => new Date(a)
                .getTime() - 
            new Date(b)
                .getTime()
        );


    return sorted;
}

export function mergeKpis(kpi: KpiElement) : KpiElement {
    logger.debug('mergeKpis');
    logger.debug(`mergeKpis, kpi: ${kpi.name}`);

    let newElement : KpiElement = {
        name: kpi.name,
        oldValues: [],
        newValues: [],
        oldLabels: [],
        newLabels: []
    };

    let done = false;
    let firstIndex = 0;
    let secondIndex = 0;

    while (!done) {
        if(firstIndex>=kpi.oldLabels.length) {
            // we are at the end of oldLabels and oldValues
            // but in newLabels and newValues are still elements
            // take only the new values and add nan to the old values
            newElement.newLabels.push(kpi.newLabels[secondIndex]);
            newElement.newValues.push(kpi.newValues[secondIndex]);
            newElement.oldLabels.push(kpi.newLabels[secondIndex]);
            newElement.oldValues.push(NaN);
            secondIndex++;
        } else if(secondIndex>=kpi.newLabels.length) {
            // we are at the end of newLabels and newValues
            // but in oldLabels and oldValues are still elements
            // take only the new values and add nan to the old values
            newElement.newLabels.push(kpi.oldLabels[firstIndex]);
            newElement.newValues.push(NaN);
            newElement.oldLabels.push(kpi.oldLabels[firstIndex]);
            newElement.oldValues.push(kpi.oldValues[firstIndex]);
            firstIndex++;
        } else if (kpi.oldLabels[firstIndex] > kpi.newLabels[secondIndex]) {
            // we got a new label that is the next item
            newElement.newLabels.push(kpi.newLabels[secondIndex]);
            newElement.newValues.push(kpi.newValues[secondIndex]);
            newElement.oldValues.push(NaN);
            secondIndex++;
        } else if (kpi.oldLabels[firstIndex] < kpi.newLabels[secondIndex]) {
            // we got an old label that is the next item
            newElement.newLabels.push(kpi.oldLabels[firstIndex]);
            newElement.newValues.push(NaN);
            newElement.oldValues.push(kpi.oldValues[secondIndex]);
            firstIndex++;
        } else if (kpi.oldLabels[firstIndex] === kpi.newLabels[secondIndex]) {
            // both labels have the same time
            newElement.newLabels.push(kpi.oldLabels[secondIndex]);
            newElement.newValues.push(kpi.newValues[secondIndex]);
            newElement.oldLabels.push(kpi.oldLabels[firstIndex]);
            newElement.oldValues.push(kpi.oldValues[firstIndex]);
            firstIndex++;
            secondIndex++;
        }

        if ((firstIndex>=kpi.oldLabels.length) && (secondIndex>=kpi.newLabels.length)) {
            done = true;
        }
    }

    logger.debug(`mergeKpis new list: ${newElement}`);
    logger.debug('mergeKpis done');

    return newElement;
}
