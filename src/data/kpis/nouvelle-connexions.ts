import { logger } from '../../util/logger-service';
import { DataService } from '../data-service';
import { KpiElement } from './types';

const NOUVELLE_CONNEXIONS_NEW_STRING =
    `  SELECT DATE_TRUNC('month', mois) AS __timestamp,
        sum(valeur) AS "SUMvaleur"
        FROM warehouse.matrice_complete_copy
        WHERE categorie = 'connections_new'
        AND mois >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
        GROUP BY DATE_TRUNC('month', mois)
        ORDER BY __timestamp ASC`;

const NOUVELLE_CONNEXIONS_NEW_STRING_SHORT =
    `  SELECT DATE_TRUNC('month', mois) AS __timestamp,
        sum(valeur) AS "SUMvaleur"
        FROM warehouse.matrice_complete_copy
        WHERE categorie = 'connections_new'
        AND mois >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
        GROUP BY DATE_TRUNC('month', mois)
        ORDER BY __timestamp ASC
        LIMIT 100;`;

const NOUVELLE_CONNEXIONS_OLD_STRING =
    `  SELECT DATE_TRUNC('month', mois) AS __timestamp,
        sum(valeur) AS "SUMvaleur"
        FROM warehouse.matrice_complete
        WHERE categorie = 'connections_new'
        AND mois >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
        GROUP BY DATE_TRUNC('month', mois)
        ORDER BY __timestamp ASC`;

const NOUVELLE_CONNEXIONS_OLD_STRING_SHORT =
    `  SELECT DATE_TRUNC('month', mois) AS __timestamp,
        sum(valeur) AS "SUMvaleur"
        FROM warehouse.matrice_complete
        WHERE categorie = 'connections_new'
        AND mois >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
        GROUP BY DATE_TRUNC('month', mois)
        ORDER BY __timestamp ASC
        LIMIT 100;`;

export async function getNouvelleConnexionsNewValues (
    dataService: DataService, 
    kpiElement: KpiElement, 
    shortQuery: boolean) {

    logger.debug('kpis:getNouvelleConnexionsNewValues');

    await dataService.warehouse
        .raw(shortQuery? NOUVELLE_CONNEXIONS_NEW_STRING_SHORT : NOUVELLE_CONNEXIONS_NEW_STRING)
        .then(entries => {
            const dataRows = entries.rows;
            for (let item of dataRows) {
                kpiElement.newValues.push(item.SUMvaleur);
                kpiElement.newLabels.push(
                    new Date(item.__timestamp)
                        .toISOString()
                        .slice(0, 10));
            }
        })
        .catch(e => {
            logger.error(`Error getNouvelleConnexionsNewValues: ${e}`);
        });

    logger.debug('kpis:getNouvelleConnexionsNewValues done');
}

export async function getNouvelleConnexionsOldValues (
    dataService: DataService, 
    kpiElement: KpiElement, 
    shortQuery: boolean) {

    logger.debug('kpis:getNouvelleConnexionsOldValues');



    await dataService.warehouse
        .raw(shortQuery? NOUVELLE_CONNEXIONS_OLD_STRING_SHORT : NOUVELLE_CONNEXIONS_OLD_STRING)
        .then(entries => {
            const dataRows = entries.rows;
            for (let item of dataRows) {
                kpiElement.oldValues.push(item.SUMvaleur);
                kpiElement.oldLabels.push(new Date(item.__timestamp)
                    .toISOString()
                    .slice(0, 10));
            }
        })
        .catch(e => {
            logger.error(`Error getNouvelleConnexionsOldValues: ${e}`);
        });

    logger.debug('kpis:getNouvelleConnexionsOldValues done');
}
