import { logger } from '../../util/logger-service';
import { DataService } from '../data-service';
import { KpiElement } from './types';

const CONNECTIONS_TOTALS_NEW_STRING = 
    `  SELECT DATE_TRUNC('month', mois) AS __timestamp,
        sum(valeur) AS "SUMvaleur"
        FROM warehouse.matrice_complete_copy
        WHERE categorie = 'connections' AND mois >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
        GROUP BY DATE_TRUNC('month', mois)
        ORDER BY __timestamp ASC`;

const CONNECTIONS_TOTALS_NEW_STRING_SHORT = 
    `  SELECT DATE_TRUNC('month', mois) AS __timestamp,
        sum(valeur) AS "SUMvaleur"
        FROM warehouse.matrice_complete_copy
        WHERE categorie = 'connections' AND mois >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
        GROUP BY DATE_TRUNC('month', mois)
        ORDER BY __timestamp ASC
        LIMIT 100;`;
    
const CONNECTIONS_TOTALS_OLD_STRING = 
    `  SELECT DATE_TRUNC('month', mois) AS __timestamp,
        sum(valeur) AS "SUMvaleur"
        FROM warehouse.matrice_complete
        WHERE categorie = 'connections' AND mois >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
        GROUP BY DATE_TRUNC('month', mois)
        ORDER BY __timestamp ASC`;

const CONNECTIONS_TOTALS_OLD_STRING_SHORT = 
    `  SELECT DATE_TRUNC('month', mois) AS __timestamp,
        sum(valeur) AS "SUMvaleur"
        FROM warehouse.matrice_complete
        WHERE categorie = 'connections' AND mois >= TO_DATE('2007-01-01', 'YYYY-MM-DD')
        GROUP BY DATE_TRUNC('month', mois)
        ORDER BY __timestamp ASC
        LIMIT 100;`;

export async function getConnexionsTotalesOldValues(dataService: DataService, kpiElement: KpiElement, shortQuery: boolean) {

    logger.debug('KpisImpl:getConnexionsTotalesOldValues');

    await dataService.warehouse
        .raw(shortQuery? CONNECTIONS_TOTALS_OLD_STRING_SHORT : CONNECTIONS_TOTALS_OLD_STRING)
        .then(entries => {
            const dataRows = entries.rows;
            for (let item of dataRows) {
                kpiElement.oldValues.push(item.SUMvaleur);
                kpiElement.oldLabels.push(
                    new Date(item.__timestamp)
                        .toISOString()
                        .slice(0, 10)
                );
            }
        })
        .catch(e => {
            logger.error(`Error getConnexionsTotalesOldValues: ${e}`);
        });

    logger.debug('KpisImpl:getConnexionsTotalesOldValues done');
}

export async function getConnexionsTotalesNewValues(dataService: DataService, kpiElement: KpiElement, shortQuery: boolean) {

    logger.debug('KpisImpl:getConnexionsTotalesNewValues');

    await dataService.warehouse
        .raw(shortQuery? CONNECTIONS_TOTALS_NEW_STRING_SHORT : CONNECTIONS_TOTALS_NEW_STRING)
        .then(entries => {
            const dataRows = entries.rows;
            for (let item of dataRows) {
                kpiElement.newValues.push(item.SUMvaleur);
                kpiElement.newLabels.push(
                    new Date(item.__timestamp)
                        .toISOString()
                        .slice(0, 10)
                );
            }
        })
        .catch(e => {
            logger.error(`Error getConnexionsTotalesNewValues: ${e}`);
        });

    logger.debug('KpisImpl:getConnexionsTotalesNewValues done');
}
