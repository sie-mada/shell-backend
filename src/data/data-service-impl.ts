import { inject, injectable } from 'inversify';
import * as Knex from 'knex';
import { ConfigService } from '../config';
import { DatabaseConfig } from '../config/types';
import { Dashboard, Section } from '../dashboards/types';
import { INJECTABLES } from '../injectables';
import { LimitedLifetimeValue } from '../util/limited-lifetime-value';
import { DataAdapter, DataItem, DataService } from './data-service';
import { StoredDashboardComment, StoredDashConfiguration, StoredSectionConfiguration } from './types';

@injectable()
export class DataServiceImpl implements DataService {

    private readonly config: DatabaseConfig;
    private readonly warehouseKnex: LimitedLifetimeValue<Knex>;

    constructor(
        @inject(INJECTABLES.Config) configService: ConfigService,
        @inject(INJECTABLES.DataAdapter) private readonly adapter: DataAdapter
    ) {
        this.config = configService.getSection<DatabaseConfig>('database');

        this.warehouseKnex = new LimitedLifetimeValue({
            lifetime: 'PT15M',
            autoTouch: true,
            initial: () => this.initKnex(),
            unref: true,
            destructor: (knex: Knex) => knex
                .destroy()
                .then()
        });
    }

    private initKnex() : Knex {
        return Knex({
            client: 'pg',
            connection: {
                host: this.config.postgresHost,
                user: this.config.postgresUser,
                password: this.config.postgresPassword,
                database: this.config.postgresDb
            }
        });
    }

    get warehouse() {
        return this.warehouseKnex.value!;
    }

    async getSections() : Promise<DataItem<StoredSectionConfiguration>[]> {
        return this.adapter.getSections();
    }

    async getDashboards() : Promise<DataItem<StoredDashConfiguration>[]> {
        return this.adapter.getDashboards();
    }

    async setDashboardConfig(dashboard: Dashboard) : Promise<DataItem<StoredDashConfiguration>> {
        return this.adapter.setDashboardConfig(mapDashboardToEntity(dashboard));
    }

    async setSectionConfig(section: Section) : Promise<DataItem<StoredSectionConfiguration>> {
        return this.adapter.setSectionConfig(mapSectionToEntity(section));
    }

    async deleteSection(section: Section) : Promise<void> {
        return this.adapter.deleteSection(section.id);
    }

    async getComments(adapter: string, remoteId: string) : Promise<DataItem<StoredDashboardComment>[]> {
        return this.adapter.getComments(adapter, remoteId);
    }

    async addComment(adapter: string, remoteId: string, comment: StoredDashboardComment) : Promise<DataItem<StoredDashboardComment>> {
        return this.adapter.addComment(adapter, remoteId, comment);
    }
}

function mapDashboardToEntity(dashboard: Partial<Dashboard>) : Partial<DataItem<StoredDashConfiguration>> {
    return {
        _id: dashboard.id,
        adapter: dashboard.adapter,
        remoteId: dashboard.remoteId,
        url: dashboard.url,
        title: dashboard.title,
        assignedTo: dashboard.section
    };
}

function mapSectionToEntity(section: Partial<Section>) : Partial<DataItem<StoredSectionConfiguration>> {
    return {
        _id: section.id,
        name: section.name,
        title: section.title,
        parent: section.parent,
        weight: section.weight
    };
}
