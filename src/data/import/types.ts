export interface DataImporterDto {
    name: string;
    label: string;
    description: string;
    example?: string;
    fileTypes?: string;
    fileStoragePath?: string;
    order: string;
}

export interface DataImporter extends DataImporterDto {
    command: string;
    commands: { [commandname: string]: string };
    output: 'sql' | 'none';
}

export function IsDataImporter(candidate: any): candidate is DataImporter {
    return typeof candidate === 'object'
        && !!candidate
        && typeof candidate.name === 'string'
        && typeof candidate.label === 'string'
        && typeof candidate.description === 'string'
        && typeof candidate.command === 'string'
        && typeof candidate.output === 'string';
}

export interface ImportError {
    errorType?: string;
    errorDescription?: string;
}

export interface RejectInfo {
    message?: string;
    userName?: string;
    rejectTime?: string;
}

export type RejectInfoList = RejectInfo[];

export interface ImportStatus {
    importId: string;
    status: 'copy_running' | 'running' | 'skipped' | 'done' | 'failed';
    start: string;
    end?: string;
    output?: string;
    result?: string;
    importErrors?: ImportError;
}

export type ImportPreResultList = ImportPreResultElement[];

export interface ImportPreResultElement {
    kpiName: string;
    oldValues: DataSet;
    newValues: DataSet;
    oldLabels: string[];
    newLabels: string[];
    commonLabels: string[];
}

export interface ImportDetail {
    kpis: ImportPreResultList;
    fileStructure: string;
    rejectInfoList: string;
}

interface DataSet {
    data: number[];
    label: string;
}

export type ImportResult = 'running' | 'done' | 'failed';

export type ImportHistoryList = ImportHistoryElement[];

export interface ImportHistoryElement {
    importId: string;
    dateStarted: string;
    importStep: string;
    importStatus: ImportResult;
    userName: string;
    type: string;
    filename: string;
}

export interface RowElement {
    row: number;
    property: string;
}

export type FileStructure = RowElement[];

