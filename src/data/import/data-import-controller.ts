import * as Router from '@koa/router';
import { mkdtemp } from 'fs';
import * as contentDisposition from 'content-disposition';
import { BAD_REQUEST, NOT_FOUND } from 'http-status-codes';
import { inject, injectable } from 'inversify';
import { Middleware } from 'koa';
import { promisify } from 'util';
import { Controller } from '../../app';
import { AuthService } from '../../auth';
import { INJECTABLES } from '../../injectables';
import { DataImportService } from './data-import-service';
import { NoSuchEntityError } from './no-such-file-error';
import { logger } from '../../util/logger-service';

const mkdtempP = promisify(mkdtemp);

@injectable()
export class DataImportController implements Controller {

    private readonly dataAdmin: Middleware;

    constructor(
        @inject(INJECTABLES.Auth) auth: AuthService,
        @inject(INJECTABLES.DataImport) private readonly service: DataImportService
    ) {
        this.dataAdmin = auth.middleware(user => (user.groups || []).indexOf('data_admins') >= 0);
    }

    registerRoutes(router: Router, middleware?: Middleware | Middleware[], prefix: string = '') {
        const middlewares = !middleware
            ? []
            : Array.isArray(middleware)
                ? middleware
                : [middleware];

        router.get(`${prefix}/data/importer`, ...middlewares, this.dataAdmin, this.getImporters.bind(this));
        router.get(`${prefix}/data/importer/:name/example`, ...middlewares, this.getExample.bind(this));
        router.post(`${prefix}/data/importer/:name/import`, ...middlewares, this.dataAdmin, this.upload.bind(this));
        router.get(`${prefix}/data/importer/:name/import/:id`, ...middlewares, this.dataAdmin, this.getImportStatus.bind(this));
        router.post(`${prefix}/data/importer/:name/importcheck`, ...middlewares, this.dataAdmin, this.importCheck.bind(this));
        router.post(`${prefix}/data/importer/:name/importpreprocess`, ...middlewares, this.dataAdmin, this.importpreprocess.bind(this));
        router.post(`${prefix}/data/importer/:name/reject`, ...middlewares, this.dataAdmin, this.importReject.bind(this));
        router.get(`${prefix}/data/importer/importhistorylist`, ...middlewares, this.dataAdmin, this.getImportHistoryList.bind(this));
        router.get(`${prefix}/data/importer/:name/download/:id`, ...middlewares, this.getExcelFile.bind(this));
        router.get(`${prefix}/data/importer/:name/importdetail/:id`, ...middlewares, this.getImportDetail.bind(this));
    }

    async getImporters(ctx: Router.RouterContext) {
        try {
            ctx.body = await this.service.getImporters();
        } catch (e) {
            ctx.throw(e);
        }
    }

    async getExample(ctx: Router.RouterContext) {
        try {
            console.log('getExample');
            const example = await this.service.getExample(ctx.params.name);
            ctx.type = example.type;
            ctx.set('Content-disposition', `attachment; filename="${example.name}"`);
            ctx.body = example.stream;
        } catch (e) {
            ctx.throw(NOT_FOUND, e);
        }
    }
    
    async getExcelFile(ctx: Router.RouterContext) {
        try {
            logger.debug('getExcelFile');
            const excelFile = await this.service.getExcelFile(ctx.params.name, ctx.params.id);
            ctx.type = excelFile.type;
            // const contentFileName = excelFile.name.replace(/\s/g, '');
            const contentDispositionHeader = contentDisposition(excelFile.name);
            logger.debug('getExcelFile, disposition header:');
            logger.debug(contentDispositionHeader);
            ctx.set('Content-disposition', contentDispositionHeader);
            ctx.body = excelFile.stream;
        } catch (e) {
            ctx.throw(NOT_FOUND, e);
        }
    }

    async getImportDetail(ctx: Router.RouterContext) {
        try {
            const importId = ctx.params.id;
            ctx.body = await this.service.getImportDetail(ctx.params.name, importId);
        } catch (e) {
            ctx.throw(NOT_FOUND, e);
        }
    }

    async upload(ctx: Router.RouterContext) {
        if (!(ctx.request.body.user)) {
            ctx.throw(BAD_REQUEST, 'Le nom ou la groupe d\'utilisateur doit être défini');
        }
        if (!(ctx.request.body.importId)) {
            ctx.throw(BAD_REQUEST, 'L\'Id de L\'importation est manquand');
        }

        try {
            const user: string = ctx.request.body.user;
            const importId = await ctx.request.body.importId;

            ctx.body = await this.service.startImport(ctx.params.name, user, importId);
        } catch (e) {
            if (e instanceof NoSuchEntityError) {
                ctx.throw(NOT_FOUND, e);
            }

            ctx.throw(e);
        }
    }

    async importCheck(ctx: Router.RouterContext) {
        if (!(ctx.request.files && ctx.request.files.file)) {
            ctx.throw(BAD_REQUEST, 'Fichier à verification de l\'importation est manquand');
        }
        if (!(ctx.request.body.user)) {
            ctx.throw(BAD_REQUEST, 'Le nom ou la groupe d\'utilisateur doit être défini');
        }
        if (!(ctx.request.body.fileStructure)) {
            ctx.throw(BAD_REQUEST, 'La structure du fichier est manquand');
        }
        if (!(ctx.request.body.checkFile)) {
            ctx.throw(BAD_REQUEST, 'CheckFile est manquand');
        }

        try {
            const file = ctx.request.files!.file;
            const user: string = await ctx.request.body.user;
            const fileStructure: string = await ctx.request.body.fileStructure;
            const checkFile: boolean = await ctx.request.body.checkFile;
            ctx.body = await this.service.checkImport(ctx.params.name, user, file.path, file.name, fileStructure, checkFile);
        } catch (e) {
            if (e instanceof NoSuchEntityError) {
                ctx.throw(NOT_FOUND, e);
            }

            ctx.throw(e);
        }
    }

    async importpreprocess(ctx: Router.RouterContext) {

        try {
            const importId = await ctx.request.body.importId;
            ctx.body = await this.service.preProcessImport(ctx.params.name, importId);

        } catch (e) {
            if (e instanceof NoSuchEntityError) {
                ctx.throw(NOT_FOUND, e);
            }

            ctx.throw(e);
        }
    }

    async importReject(ctx: Router.RouterContext) {
        if (!(ctx.request.body.user)) {
            ctx.throw(BAD_REQUEST, 'Le nom ou la groupe d\'utilisateur doit être défini');
        }
        try {
            const importId = await ctx.request.body.importId;
            const rejectText = await ctx.request.body.rejectText;
            const userName = await ctx.request.body.user;
            const rejectTime = await ctx.request.body.time;

            ctx.body = await this.service.rejectImport (
                ctx.params.name, 
                importId, 
                rejectText, 
                userName,
                rejectTime);

        } catch (e) {
            if (e instanceof NoSuchEntityError) {
                ctx.throw(NOT_FOUND, e);
            }

            ctx.throw(e);
        }
    }

    async getImportHistoryList(ctx: Router.RouterContext) {
        console.log('getImportHistoryList');
        try {
            ctx.body = await this.service.getImportHistoryList();
        } catch (e) {
            ctx.throw(e);
        }
    }

    getImportStatus(ctx: Router.RouterContext) {
        try {
            ctx.body = this.service.getImportStatus(ctx.params.name, ctx.params.id);
        } catch (e) {
            if (e instanceof NoSuchEntityError) {
                ctx.throw(NOT_FOUND, e);
            }

            ctx.throw(e);
        }
    }
}
