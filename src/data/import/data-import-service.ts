import { Readable } from 'stream';
import { DataImporterDto, ImportHistoryList, ImportStatus, ImportPreResultList, ImportDetail } from './types';

export interface DataExampleDto {
    type: string;
    name: string;
    stream: Readable;
}

export interface DataImportService {
    getImporters(): Promise<DataImporterDto[]>;
    getExample(importer: string): Promise<DataExampleDto>;

    startImport(type: string, user: string, importId: string): Promise<ImportStatus>;
    checkImport(type: string, user: string, filePath: string, fileOrigName: string, fileStructure: string, checkFile: boolean): Promise<ImportStatus>;
    preProcessImport(type: string, importId: string): Promise<ImportPreResultList>;
    rejectImport(type: string, importId: string, rejectText: string,userName: string, rejectTime: string): Promise<boolean>;
    getImportStatus(type: string, id: string): ImportStatus;
    getImportHistoryList(): Promise<ImportHistoryList>;
    getExcelFile(importer: string, importId: string): Promise<DataExampleDto>;
    getImportDetail(importer: string, importId: string): Promise<ImportDetail>;
}
