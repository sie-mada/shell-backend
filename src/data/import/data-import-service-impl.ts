import { ChildProcess, spawn } from 'child_process';
import { createReadStream, mkdtemp, readdir, rename, stat, unlink } from 'fs';
import { inject, injectable } from 'inversify';
import { pickAll, template } from 'lodash/fp';
import { contentType } from 'mime-types';
import * as moment from 'moment';
import { tmpdir } from 'os';
import { join as joinPath } from 'path';
import * as path from 'path';
import { promisify } from 'util';
import { v4 as uuidV4 } from 'uuid';
import { ConfigService } from '../../config';
import { BaseConfig, DataImportConfig } from '../../config/types';
import { INJECTABLES } from '../../injectables';
import { DataService } from '../data-service';
import { DataExampleDto, DataImportService } from './data-import-service';
import { NoSuchEntityError } from './no-such-file-error';
import { DataImporter, DataImporterDto, ImportHistoryElement, ImportHistoryList, ImportStatus, ImportResult, IsDataImporter, ImportPreResultList, 
    ImportPreResultElement, ImportError, ImportDetail, RejectInfo, RejectInfoList } from './types';
import { logger } from '../../util/logger-service';
import * as kpis from '../kpis/kpis';
import { KpiElement, KpiList } from '../kpis/types';
import * as fs from 'fs';
import { mergeKpis } from '../kpis/merge-kpis';

const mkdtempP = promisify(mkdtemp);
const readdirP = promisify(readdir);
const renameP = promisify(rename);
const statP = promisify(stat);

interface ImportProcess {
    id: string;
    type: string;
    start: moment.Moment;
    end?: moment.Moment;
    status: 'copy_running' | 'running' | 'skipped' | 'done' | 'failed';
    process: ChildProcess | null;
    file: string;
    exitCode?: number;
    sqlOutput: boolean;
    output?: string;
    result?: string;
    errors?: ImportError;
    user?: string;
    fileOrigName: string;
    fileStructure: string;
    checkFile: boolean;
}

interface ReturnInformation {
    result: string;
    errorDescription: ImportError;
}

@injectable()
export class DataImportServiceImpl implements DataImportService {

    private readonly baseConfig: BaseConfig;
    private readonly config: DataImportConfig;
    private readonly importProcessRegistry: Map<string, ImportProcess> = new Map();

    constructor(
        @inject(INJECTABLES.Config) configService: ConfigService,
        @inject(INJECTABLES.Data) private readonly dataService: DataService
    ) {
        this.baseConfig = configService.getSection<BaseConfig>('base');
        this.config = configService.getSection<DataImportConfig>('dataImport');
    }

    async getImporters() {
        const apiHost = this.baseConfig.apiHostName
            || (this.baseConfig.rootHostName
                ? `api.${this.baseConfig.rootHostName}`
                : 'localhost'
            );
        const protocol = this.baseConfig.rootProtocol || 'http';

        if (!(await statP(this.config.importersDir)).isDirectory()) {
            logger.warn(' getImporters: Importers dicrectory missing');

            return [];
        }

        const importerDirs = (await readdirP(this.config.importersDir, { withFileTypes: true }))
            .filter(dirEnt => dirEnt.isDirectory());

        const importers = await Promise.all(
            importerDirs.map(
                dirEnt => this
                    .loadDataImporter(dirEnt.name)
                    .catch(() => null)
            )
        );

        return (importers
            .filter(Boolean) as DataImporter[])
            .map(importer => {
                const mapped: DataImporterDto = {
                    name: importer.name,
                    label: importer.label,
                    description: importer.description,
                    order: importer.order
                };

                if (importer.fileTypes) {
                    mapped.fileTypes = importer.fileTypes;
                }

                if (importer.example) {
                    mapped.example = `${protocol}://${apiHost}/data/importer/${importer.name}/example`;
                }

                return mapped;
            });
    }

    async getExample(importer: string): Promise<DataExampleDto> {
        if (/[/\\]|\.\./.test(importer)) {
            throw new Error('Nom d\'importeur illégal');
        }

        try {
            const importerDefinition = await this.loadDataImporter(importer);
            if (!importerDefinition.example) {
                throw new Error();
            }

            return {
                type: contentType(importerDefinition.example) || 'application/octet-stream',
                name: importerDefinition.example,
                stream: createReadStream(joinPath(this.config.importersDir, importer, importerDefinition.example), { autoClose: true })
            };
        } catch (e) {
            throw new Error('Impossible de retrouver l\'example');
        }
    }

    async getImportDetail(importer: string, importId: string): Promise<ImportDetail> {
        if (/[/\\]|\.\./.test(importer)) {
            throw new Error('Nom d\'importeur illégal');
        }
        if (!importId) {
            throw new Error('Id d\'importation illégal');
        }
    
        try {
            const importerDefinition = await this.loadDataImporter(importer);
            const fileStoragePath = importerDefinition.fileStoragePath!;
            const kpisFileName = `import_${importId}/kpis_${importId}.json`;
            const kpisJoinedPath = joinPath(fileStoragePath, kpisFileName);      
            const kpiValues : KpiList = [];
            kpis.loadKpis(importId, kpisJoinedPath, kpiValues);
            let list: ImportPreResultList = [];

            for (let kpiElement of kpiValues) {
                let newCommonValues = mergeKpis(kpiElement);
                const ele: ImportPreResultElement = {
                    kpiName: kpiElement.name,
                    oldValues: { data: newCommonValues.oldValues, label: 'Avant L\'importation' },
                    newValues: { data: newCommonValues.newValues, label: 'Aprés L\'importation' },
                    oldLabels: kpiElement.oldLabels,
                    newLabels: kpiElement.newLabels,
                    commonLabels: newCommonValues.newLabels
                };
                list.push(ele);
            }

            const fileStructureFileName = `import_${importId}/structure_${importId}.json`;
            const fileStructureJoinedPath = joinPath(fileStoragePath, fileStructureFileName); 
            const fileStructure = this.loadFileStructure(fileStructureJoinedPath);

            const rejectInfoFileName = `import_${importId}/reject_${importId}.json`;
            const rejectJoinedPath = joinPath(fileStoragePath, rejectInfoFileName);
            
            const rejectInfoListStruct : RejectInfoList = this.loadRejectInfoList(rejectJoinedPath);
            const rejectInfoList : string = JSON.stringify(rejectInfoListStruct);
            const importDetail: ImportDetail = {
                kpis: list, 
                fileStructure,
                rejectInfoList
            };
            const ret = new Promise<ImportDetail>(resolve => {
                logger.debug('getImportDetail resolve');
                resolve(importDetail);
            });
            logger.debug('getImportDetail done');

            return ret;
        } catch (e) {
            logger.error('Impossible d\'obtenir le fichier d\'kpis ou fileStructure');
            throw new Error('Impossible d\'obtenir le fichier d\'kpis ou fileStructure');
        }
    }
    
    private loadFileStructure (dataPath: string): string {
            let rawData = fs.readFileSync(dataPath);
            const structure = JSON.stringify(JSON.parse(rawData.toString()));
            
            return structure;
    }

    // private loadRejectInfo (dataPath: string): string {
    //     let rawData = fs.readFileSync(dataPath);
    //     const structure = JSON.stringify(JSON.parse(rawData.toString()));
        
    //     return structure;
    // }

    private loadRejectInfoList (dataPath: string): RejectInfoList {
        let list : RejectInfoList = [];

        if (fs.existsSync(dataPath)) {
            let rawData = fs.readFileSync(dataPath);            
            list = JSON.parse(rawData.toString());
        }

        return list;
    }

    async startImport (type: string, user: string, importId: string): Promise<ImportStatus> {
        logger.debug(`DISI:startImport type: ${type} importId: ${importId}`);
        const importer = await this.loadDataImporter(type);
        const fileStoragePath = importer.fileStoragePath!;
        const joinedPath = joinPath(fileStoragePath, `import_${importId}`);

        const excelFile = this.getFileInDir(joinedPath);

        const correctedFilePath = joinPath(joinedPath, excelFile);

        if (!excelFile) {
            throw new Error('Fichier d\'importation non trouvé');
        }


        const commandTemplate = template(importer.command);
        const command = commandTemplate({
            file: correctedFilePath,
            command: 'start_import',
            user,
            importid: importId,
            fileStructure: undefined,
            checkFile: undefined,
            ...pickAll(['dbHost', 'dbPort', 'dbName', 'dbUser', 'dbPass'], this.config)
        });

        const importerDir = this.getImporterDir(type);
        const process = spawn(command, { shell: true, cwd: importerDir, stdio: 'pipe' });
        const start = moment();

        const description: ImportProcess = {
            id: importId,
            type,
            start,
            process,
            file: correctedFilePath,
            fileStructure: '',
            fileOrigName: '',
            checkFile: false,
            sqlOutput: importer.output === 'sql',
            status: 'running'
        };

        this.importProcessRegistry.set(description.id, description);
        this.trackImportProcess(description.id);

        return this.getStatusForImportProcess(description);
    }

    async checkImport(type: string, user: string, filePath: string, fileOrigName: string, fileStructure: string, checkFile: boolean): Promise<ImportStatus> {
        logger.debug('data-import:checkImport');
        const importer = await this.loadDataImporter(type);
        const id = uuidV4();
        logger.debug(`data-import:checkImport filePath: ${filePath}`);
        logger.debug(`data-import:checkImport fileOrigName: ${fileOrigName}`);
        const tempDir = await mkdtempP(joinPath(tmpdir(), 'meeh-import'));
        const correctedFilePath = joinPath(tempDir, fileOrigName);
        logger.debug(`data-import:checkImport xls file path: ${correctedFilePath}`);

        await renameP(filePath, correctedFilePath);
        let newJsonName = path.parse(fileOrigName).name;
        newJsonName += '.json';
        const jsonFilePath = joinPath(tempDir, newJsonName);
        logger.debug(jsonFilePath);
        fs.writeFileSync(jsonFilePath, fileStructure);
        logger.debug(`data-import:checkImport write json to ${jsonFilePath}`);

        const commandTemplate = template(importer.command);
        const cmd = commandTemplate({
            file: correctedFilePath,
            command: 'check_import',
            user,
            importid: id,
            fileStructure: jsonFilePath,
            checkFile,
            ...pickAll(['dbHost', 'dbPort', 'dbName', 'dbUser', 'dbPass'], this.config)
        });
        const cmdForCopy = commandTemplate({
            file: correctedFilePath,
            command: 'start_import_for_copy',
            user,
            importid: id,
            fileStructure: jsonFilePath,
            checkFile,
            ...pickAll(['dbHost', 'dbPort', 'dbName', 'dbUser', 'dbPass'], this.config)
        });

        const importerDir = this.getImporterDir(type);
        const process = spawn(cmd, { shell: true, cwd: importerDir, stdio: 'pipe' });
        const start = moment();

        const description: ImportProcess = {
            id,
            type,
            start,
            process,
            file: correctedFilePath,
            fileOrigName,
            fileStructure,
            checkFile,
            sqlOutput: importer.output === 'sql',
            status: 'running'
        };

        this.importProcessRegistry.set(description.id, description);
        this.trackCheckImportProcess(description, importerDir);

        return this.getStatusForImportProcess(description);
    }

    startImportForCopy = async (importId: string, type: string, user: string, filePath: string, fileOrigName: string, fileStructure: string, checkFile: boolean): Promise<ImportStatus> => {
        logger.debug('data-import:startImportForCopy');
        logger.debug(`data-import:startImportForCopy filePath: ${filePath}`);
        logger.debug(`data-import:startImportForCopy fileOrigName: ${fileOrigName}`);
        const importer = await this.loadDataImporter(type);
        const id = importId;
        const tempDir = await mkdtempP(joinPath(tmpdir(), 'meeh-import'));
        const correctedFilePath = joinPath(tempDir, fileOrigName);
        await renameP(filePath, correctedFilePath);
        logger.debug(`data-import:startImportForCopy xls file path: ${correctedFilePath}`);
        let newJsonName = path.parse(fileOrigName).name;
        newJsonName += '.json';
        const jsonFilePath = joinPath(tempDir, newJsonName);
        fs.writeFileSync(jsonFilePath, fileStructure);
        logger.debug(`data-import:startImportForCopy write json to ${jsonFilePath}`);

        const commandTemplate = template(importer.command);
        const cmd = commandTemplate({
            file: correctedFilePath,
            command: 'start_import_for_copy',
            user,
            importid: id,
            fileStructure: jsonFilePath,
            checkFile,
            ...pickAll(['dbHost', 'dbPort', 'dbName', 'dbUser', 'dbPass'], this.config)
        });

        const importerDir = this.getImporterDir(type);
        const process = spawn(cmd, { shell: true, cwd: importerDir, stdio: 'pipe' });
        const start = moment();

        const description: ImportProcess = {
            id,
            type,
            start,
            process,
            file: correctedFilePath,
            user,
            fileOrigName,
            fileStructure,
            checkFile,
            sqlOutput: importer.output === 'sql',
            status: 'copy_running'
        };

        this.importProcessRegistry.set(description.id, description);
        this.trackCopyImportProcess(description.id);
        const result =  this.getStatusForImportProcess(description);
        const ret = new Promise<ImportStatus>(resolve => {
            logger.debug('data-import:startImportForCopy resolve');
            resolve(result);
        });
        logger.debug(`data-import:startImportForCopy ${description.id} done`);

        return ret;
    };


    async preProcessImport(type: string, importId: string): Promise<ImportPreResultList> {
        logger.debug('preProcessImport');
        let list: ImportPreResultList = [];

        const kpiValueList: KpiList = await kpis.getKpiValues(importId, this.dataService);

        for (let kpiElement of kpiValueList) {
            let newCommonValues = mergeKpis(kpiElement);
            // this can be used for debugging the kpis
            // const kpiOldFileName = `DEBUG_${kpiElement.name}.json`;
            // const kpiNewFileName = `DEBUG_${kpiElement.name}_merged.json`;
            // const kpiOldJsonString = JSON.stringify(kpiElement);
            // const kpiNewJsonString = JSON.stringify(newCommonValues);
            // fs.writeFileSync(kpiOldFileName, kpiOldJsonString);
            // fs.writeFileSync(kpiNewFileName, kpiNewJsonString);

            const ele: ImportPreResultElement = {
                kpiName: kpiElement.name,
                oldValues: { data: newCommonValues.oldValues, label: 'Avant L\'importation' },
                newValues: { data: newCommonValues.newValues, label: 'Aprés L\'importation' },
                oldLabels: kpiElement.oldLabels,
                newLabels: kpiElement.newLabels,
                commonLabels: newCommonValues.newLabels
            };
            list.push(ele);
        }

        try {
            const importerDefinition = await this.loadDataImporter(type);
            const fileStoragePath = importerDefinition.fileStoragePath!;
            const fileName = `import_${importId}/kpis_${importId}.json`;
            const joinedPath = joinPath(fileStoragePath, fileName);  
            kpis.storeKpis(importId, kpiValueList, joinedPath);
        } catch (e) {
            throw new Error('Impossible de sauver le fichier d\'kpis');
        }
        
        const ret = new Promise<ImportPreResultList>(resolve => {
            logger.debug('preProcessImport resolve');
            resolve(list);
        });
        logger.debug('preProcessImport done');

        return ret;
    }

    async rejectImport (
        importer: string, 
        importId: string, 
        rejectText: string,
        userName: string,
        rejectTime: string)
        : Promise<boolean> {

        logger.debug('rejectImport');
        const importerDefinition = await this.loadDataImporter(importer);
        const fileStoragePath = importerDefinition.fileStoragePath!;
        const rejectInfoFileName = `import_${importId}/reject_${importId}.json`;
        const rejectJoinedPath = joinPath(fileStoragePath, rejectInfoFileName);

        let infoString = '';
        let rejectInfoList : RejectInfoList = this.loadRejectInfoList(rejectJoinedPath);
        const newInfo : RejectInfo = {
            message: rejectText,
            userName,
            rejectTime
        };
        rejectInfoList.push(newInfo);

        const rejectInfoString = JSON.stringify(rejectInfoList);
        fs.writeFileSync(rejectJoinedPath, rejectInfoString);
        logger.debug('rejectImport done');

        return new Promise<boolean>(resolve => {
            logger.debug('rejectImport resolve');
            resolve(true);
        });
    }


    getImportStatus(type: string, id: string): ImportStatus {
        logger.debug('getImportStatus for id ', id);
        const process = this.importProcessRegistry.get(id);
        if (!process || process.type !== type) {
            throw new NoSuchEntityError(`${type}/${id}`);
        }

        return this.getStatusForImportProcess(process);
    }

    async getImportHistoryList(): Promise<ImportHistoryList> {
        logger.debug('DIS:getImportHistoryList');
        let list: ImportHistoryList = [];

        interface DbEntry {
            import_id: string;
            timestamp: Date;
            importStep: string;
            import_status: string;
            user: string;
            resultInformation: string;
            id: number;
            type: string;
            filename: string;
        }

        await this.dataService.warehouse<DbEntry>('imports_status')
            .select('*')
            .join('imports', 'imports_status.import_id', '=', 'imports.id')
            .whereRaw('import_id not in (select import_id from imports_status where import_step=\'import\') ')
            .orWhere('import_step', 'import')
            .then(entries => {
                for (let entry of entries) {
                    const newElement: ImportHistoryElement = {
                        importId: entry.import_id,
                        dateStarted: entry.timestamp,
                        importStep: entry.import_step,
                        importStatus: entry.import_status as ImportResult,
                        userName: entry.user,
                        type: this.formatImporterType(entry.type),
                        filename: entry.filename
                    };
                    list.push(newElement);
                }
            });
        
        list.sort((a, b) => new Date(b.dateStarted).getTime() - new Date(a.dateStarted).getTime());
        
        const listlen = list.length;
        logger.debug(`DIS:getImportHistoryList, list len: ${listlen}`);

        const ret = new Promise<ImportHistoryList>(resolve => {
            logger.debug('rDIS:getImportHistoryList esolve list');
            resolve(list);
        });

        return ret;
    }

    private formatImporterType(type: string) {
        return type.replace('_', '-')
        .toLowerCase();
    }

    async getExcelFile(importer: string, importId: string): Promise<DataExampleDto> {
        if (/[/\\]|\.\./.test(importer)) {
            throw new Error('Nom d\'importeur illégal');
        }

        if (!importId) {
            throw new Error('Id d\'importation illégal');
        }

        try {
            const importerDefinition = await this.loadDataImporter(importer);
            const fileStoragePath = importerDefinition.fileStoragePath!;
            const joinedPath = joinPath(fileStoragePath, `import_${importId}`);

            const name = this.getFileInDir(joinedPath);
            if (!name) {
                throw new Error('Fichier d\'importation non trouvé');
            }

            const type : string = contentType(name) || 'application/octet-stream';

            return {
                type,
                name,
                stream: createReadStream(joinPath(fileStoragePath, `import_${importId}`, name), { autoClose: true })
            };
        } catch (e) {
            throw new Error('Impossible de retrouver le fichier d\'importation');
        }
    }

    private getFileInDir(dirPath: string): string {
        let excelFile = '';
        fs.readdirSync(dirPath)
        .forEach(file => {
            if (file.endsWith('.xlsx') || file.endsWith('.xsl')) {
                excelFile = file;
                
                return excelFile;
            }
        });

        return excelFile;
    }

    private readonly trackCheckImportProcess = (description: ImportProcess, importerDir: string) => {
        logger.debug('trackCheckImportProcess id: ', description.id);
        const processDesc = this.importProcessRegistry.get(description.id)!;
        const process = processDesc.process!;
        const outputChunks: Buffer[] = [];
        process.stderr!.on(
            'data',
            chunk => logger.error(`error from check ${chunk.toString()}`)
        );
        process.stdout!.on(
            'data',
            chunk => outputChunks.push(
                Buffer.isBuffer(chunk)
                    ? chunk
                    : typeof chunk === 'string'
                        ? Buffer.from(chunk, 'utf8')
                        : Buffer.from(chunk)
            )
        );

        function cleanup() {
            outputChunks.splice(0);
            process.unref();
            unlink(processDesc.file, () => undefined);
        }

        process.on('close', (code, signal) => {
            logger.debug('trackCheckImportProcess close');
            processDesc.end = moment();
            processDesc.status = code === 0 ? 'done' : 'failed';
            processDesc.exitCode = code;
            const tmpBuffer = Buffer.concat(outputChunks)
                .toString('utf8');
            if (signal) {
                logger.error(`Error Import check process was killed by signal ${signal}`);
            }
            if (!code && !signal && processDesc.sqlOutput) {
                const endOfInfoString: string = '_endOfInfo_';
                const indexEndOfData = tmpBuffer.indexOf(endOfInfoString);
                let returnInfoString = tmpBuffer.substr(0, indexEndOfData);
                logger.info(`check import returninfo: ${returnInfoString}`);

                if (!returnInfoString) {
                    logger.error('no valid import check return answer from importer');
                    processDesc.status = 'failed';
                    processDesc.exitCode = Number.MIN_SAFE_INTEGER;
                    cleanup();

                    return;
                }

                const returnObject: ReturnInformation = JSON.parse(returnInfoString);

                if (returnObject) {
                    if (returnObject.result !== 'done' && returnObject.result !== 'skipped') {
                        logger.error('Import process failed');
                        logger.error('Error(s): ', returnObject.errorDescription);
                        processDesc.status = 'failed';
                        processDesc.exitCode = Number.MIN_SAFE_INTEGER;
                        processDesc.errors = returnObject.errorDescription;
                    } else {
                        logger.info('Import process successful');
                        processDesc.output = tmpBuffer.substr(indexEndOfData + endOfInfoString.length);
                        logger.debug('start sql processing');
                        processDesc.status = 'running';
                        processDesc.end = undefined;
                        this.executeProcessSql(processDesc, returnObject.result, false);
                        logger.debug('sql processing started');
                        setTimeout (
                            this.startImportForCopy,1000, description.id, description.type, description.user, description.file, 
                            description.fileOrigName, description.fileStructure, description.checkFile
                        );
                    }
                } else {
                    logger.error('Error: no valid process return');
                    processDesc.status = 'failed';
                    processDesc.exitCode = Number.MIN_SAFE_INTEGER;
                }
            } else {
                logger.error('Error Import returned error code ', code);
                cleanup();
            }
        });

        process.on('error', err => {
            logger.error(err);
            processDesc.end = moment();
            processDesc.status = 'failed';
            processDesc.exitCode = Number.MIN_SAFE_INTEGER;

            cleanup();
        });
    };

    private trackImportProcess(id: string) {
        logger.debug('trackimportprocess');
        const processDesc = this.importProcessRegistry.get(id)!;
        const process = processDesc.process!;
        const outputChunks: Buffer[] = [];
        let numberBytes = 0;
        process.stderr!.on(
            'data',
            chunk => logger.error(`error from import: ${chunk.toString()}`)
        );
        process.stdout!.on(
            'data',
            chunk => {
                // logger.debug(`trackimportprocess data, len: ${chunk.length}`);
                // logger.debug(chunk.toString('utf8'));
                numberBytes += chunk.length;
                outputChunks.push(
                    Buffer.isBuffer(chunk)
                        ? chunk
                        : typeof chunk === 'string'
                            ? Buffer.from(chunk, 'utf8')
                            : Buffer.from(chunk)
                );
            }
        );

        function cleanup() {
            outputChunks.splice(0);
            process.unref();
            unlink(processDesc.file, () => undefined);
        }

        process.on('close', (code, signal) => {
            logger.debug('trackImportProcess close, code: ', code, 'signal: ', signal);
            processDesc.end = moment();
            processDesc.status = code === 0 ? 'done' : 'failed';
            processDesc.exitCode = code;
            const tmpBuffer = Buffer.concat(outputChunks)
                .toString('utf8');
            logger.debug('tmpbuffer: ', tmpBuffer);
            if (signal) {
                logger.error(`Import process was killed by signal ${signal}`);
            }
            if (!code && !signal && processDesc.sqlOutput) {
                const endOfInfoString: string = '_endOfInfo_';
                const indexEndOfData = tmpBuffer.indexOf(endOfInfoString);
                let returnInfoString = tmpBuffer.substr(0, indexEndOfData);
                logger.info(`import returninfo: ${returnInfoString}`);

                if (!returnInfoString) {
                    logger.error('no valid import return answer from importer');
                    processDesc.status = 'failed';
                    processDesc.exitCode = Number.MIN_SAFE_INTEGER;
                    cleanup();

                    return;
                }

                try {
                    const returnObject: ReturnInformation = JSON.parse(returnInfoString);
                    if (returnObject) {
                        if (returnObject.result !== 'done' && returnObject.result !== 'skipped') {
                            logger.error('Import process failed');
                            logger.error('Error: ', returnObject.errorDescription);
                            processDesc.status = 'failed';
                            processDesc.exitCode = Number.MIN_SAFE_INTEGER;
                        } else {
                            logger.info('Import process successful');
                            processDesc.output = tmpBuffer.substr(indexEndOfData + endOfInfoString.length);
                            logger.info('start sql processing');
                            processDesc.status = 'running';
                            processDesc.end = undefined;
                            this.executeProcessSql(processDesc, returnObject.result, true);
                            logger.debug('sql processing done');
                        }
                    } else {
                        logger.error('Error: no valid process return');
                        processDesc.status = 'failed';
                        processDesc.exitCode = Number.MIN_SAFE_INTEGER;
                    }
                } catch (e) {
                    logger.error('Erreur en importateur');
                    processDesc.status = 'failed';
                    processDesc.exitCode = Number.MIN_SAFE_INTEGER;
                }
            } else {
                logger.error('Error Import returned error code ', code);
                cleanup();
            }
            logger.debug('process-handling for close done');
        });

        process.on('error', err => {
            logger.debug('trackImportProcess error');
            logger.error(err);
            processDesc.end = moment();
            processDesc.status = 'failed';
            processDesc.exitCode = Number.MIN_SAFE_INTEGER;

            cleanup();
        });
    }

    private trackCopyImportProcess(id: string) {
        logger.debug('trackCopyImportProcess');
        const processDesc = this.importProcessRegistry.get(id)!;
        const process = processDesc.process!;
        const outputChunks: Buffer[] = [];
        let numberBytes = 0;
        process.stderr!.on(
            'data',
            chunk => logger.error(`trackCopyImportProcess error from import ${chunk.toString()}`)
        );
        process.stdout!.on(
            'data',
            chunk => {
                logger.debug(`trackCopyImportProcess data, len: ${chunk.length}`);
                numberBytes += chunk.length;
                outputChunks.push(
                    Buffer.isBuffer(chunk)
                        ? chunk
                        : typeof chunk === 'string'
                            ? Buffer.from(chunk, 'utf8')
                            : Buffer.from(chunk)
                );
            }
        );

        function cleanup() {
            outputChunks.splice(0);
            process.unref();
            unlink(processDesc.file, () => undefined);
        }

        process.on('close', (code, signal) => {
            logger.info('trackCopyImportProcess close, code: ', code, 'signal: ', signal);
            processDesc.end = moment();
            processDesc.status = code === 0 ? 'done' : 'failed';
            processDesc.exitCode = code;
            const tmpBuffer = Buffer.concat(outputChunks)
                .toString('utf8');
            logger.debug('trackCopyImportProcess tmpbuffer: ', tmpBuffer);
            if (signal) {
                logger.error(`trackCopyImportProcess Import process was killed by signal ${signal}`);
            }
            if (!code && !signal && processDesc.sqlOutput) {
                // logger.debug('extract return info');
                const endOfInfoString: string = '_endOfInfo_';
                const indexEndOfData = tmpBuffer.indexOf(endOfInfoString);
                let returnInfoString = tmpBuffer.substr(0, indexEndOfData);
                logger.info(`import returninfo: ${returnInfoString}`);

                if (!returnInfoString) {
                    logger.error('no valid import return answer from importer');
                    processDesc.status = 'failed';
                    processDesc.exitCode = Number.MIN_SAFE_INTEGER;
                    cleanup();

                    return;
                }

                try {
                    const returnObject: ReturnInformation = JSON.parse(returnInfoString);
                    if (returnObject) {
                        if (returnObject.result !== 'done' && returnObject.result !== 'skipped') {
                            logger.error('trackCopyImportProcess Import process failed');
                            logger.error('trackCopyImportProcess Error: ', returnObject.errorDescription);
                            processDesc.status = 'failed';
                            processDesc.exitCode = Number.MIN_SAFE_INTEGER;
                        } else {
                            logger.info('trackCopyImportProcess Import process successful');
                            processDesc.output = tmpBuffer.substr(indexEndOfData + endOfInfoString.length);
                            logger.info('trackCopyImportProcess start sql processing');
                            processDesc.status = 'running';
                            processDesc.end = undefined;
                            this.executeProcessSql(processDesc, returnObject.result, true);
                            logger.info('trackCopyImportProcess start sql processing done');
                        }
                    } else {
                        logger.error('Error: trackCopyImportProcess no valid process return');
                        processDesc.status = 'failed';
                        processDesc.exitCode = Number.MIN_SAFE_INTEGER;
                    }
                } catch (e) {
                    logger.error('Erreur en importateur');
                    processDesc.status = 'failed';
                    processDesc.exitCode = Number.MIN_SAFE_INTEGER;
                }
            } else {
                logger.error('Error trackCopyImportProcess  Import returned error code ', code);
                cleanup();
            }
            logger.debug('trackCopyImportProcess process-handling for close done');
        });

        process.on('error', err => {
            logger.error('trackCopyImportProcess error');
            logger.error(err);
            processDesc.end = moment();
            processDesc.status = 'failed';
            processDesc.exitCode = Number.MIN_SAFE_INTEGER;

            cleanup();
        });
    }

    private getStatusForImportProcess(importProcess: ImportProcess): ImportStatus {
        const status: ImportStatus = {
            importId: importProcess.id,
            start: importProcess.start.format(),
            status: importProcess.status
        };

        if (importProcess.end) {
            status.end = importProcess.end.format();
            status.output = importProcess.output;
            status.result = importProcess.result;
            status.importErrors = importProcess.errors;
        }

        logger.debug(`getStatusForImportProcess, id: ${status.importId} end: ${status.end} result: ${status.result}`);

        return status;
    }

    private async loadDataImporter(name: string): Promise<DataImporter> {
        const filename = joinPath(this.getImporterDir(name), 'manifest.json');
        if (!(await statP(filename)).isFile()) {
            throw new NoSuchEntityError(filename);
        }

        const contents = require(filename);
        if (!IsDataImporter(contents)) {
            throw new Error(`Manifest invalide pour l'importeur ${name}`);
        }

        return contents;
    }

    private getImporterDir(name: string) {
        return joinPath(this.config.importersDir, name);
    }

    private async executeProcessSql(proc: ImportProcess, result: any, isFinalStep : boolean) {
        await this.dataService.warehouse
            .transaction()
            .then(async trx => {
                try {
                    proc.output = await trx.raw(proc.output || '');
                    trx.commit();
                    // change process status only if it is the final step of the processing chain of commands
                    if (isFinalStep) {
                        proc.status = result;
                    }
                    logger.debug(`executeProcessSql, proc status set to ${proc.status}`);
                } catch (e) {
                    trx.rollback(e);
                    proc.status = 'failed';
                    proc.output = e.message || e;
                    logger.error(`DB Error at executeProcessSql: ${proc.output}`);
                }
                proc.end = moment();
                logger.debug(`executeProcessSql proc done at ${proc.end}`);
            })
            .then(inserts => {
                logger.debug('transaction done');
                logger.debug(`ìnserts: ${inserts}`);
            })
            .catch (e => {
                logger.error(e);
                logger.error(`Exception at executeProcessSql: ${e}`);
            });

        logger.info('executeProcessSql done');
    }

}

