import { inject, injectable, multiInject } from 'inversify';
import { omitAll } from 'lodash/fp';
import * as uuid from 'uuid';
import { DataItem, DataService } from '../data/data-service';
import { StoredDashConfiguration, StoredSectionConfiguration } from '../data/types';
import { INJECTABLES } from '../injectables';
import { DashboardService, RemoteDashboardAdapter, SectionsData, UNASIGNED_SECTION } from './dashboard-service';
import { Dashboard, DashboardComment, isSection, Section } from './types';

@injectable()
export class DashboardServiceImpl implements DashboardService {

    private rootSections?: Section[];
    private sectionsHash?: Map<string, Section>;
    private dashboardHash?: Map<string, Dashboard>;

    constructor(
        @inject(INJECTABLES.Data) private readonly dataService: DataService,
        @multiInject(INJECTABLES.DashboardAdapter) private readonly adapters: RemoteDashboardAdapter[]
    ) {
    }

    invalidateCache() {
        this.rootSections = this.sectionsHash = this.dashboardHash = undefined;
    }

    async getSections() : Promise<SectionsData> {
        if (!this.rootSections || !this.sectionsHash || !this.dashboardHash) {
            const [sectionsHash, rootSections] = await this.prepareSections();
            const [dashboardHash, assignedRootSections] = await this.assignDashboards(sectionsHash, rootSections);
            this.sectionsHash = sectionsHash;
            this.dashboardHash = dashboardHash;
            this.rootSections = assignedRootSections;
        }

        return {
            rootSections: this.rootSections,
            sectionsHash: this.sectionsHash,
            dashboardHash: this.dashboardHash
        };
    }

    private async prepareSections() : Promise<[Map<string, Section>, Section[]]> {
        const sectionsHash = (await this.dataService.getSections())
            .map(storedSectionToSection)
            .reduce((map, section) => map.set(section.name, section), new Map<string, Section>());

        sectionsHash.set(
            UNASIGNED_SECTION,
            {
                id: UNASIGNED_SECTION,
                name: UNASIGNED_SECTION,
                title: 'Non attribué',
                indicators: [],
                subSections: []
            }
        );

        const rootSections: Section[] = [];

        for (const section of sectionsHash.values()) {
            if (section.parent) {
                const parent = sectionsHash.get(section.parent);
                if (!parent) {
                    throw new Error(`Configuration invalide pour section ${section.name}: Section maîtresse ${section.parent} inconnue`);
                }

                parent.subSections.push(section);
            } else {
                rootSections.push(section);
            }
        }

        this.sortSections(rootSections);

        return [sectionsHash, rootSections];
    }

    private sortSections(sections: Section[]) {
        sections.sort((a, b) => ((a.weight || 0) - (b.weight || 0)) || a.title.localeCompare(b.title));
        sections.forEach(section => {
            this.sortSections(section.subSections);
            section.indicators.sort((a, b) => a.title.localeCompare(b.title));
        });
    }

    private async assignDashboards(sectionsHash: Map<string, Section>, rootSections: Section[]) : Promise<[Map<string, Dashboard>, Section[]]> {
        const [remote, configured] = await Promise.all([this.loadRemoteDashboards(), this.loadConfiguredDashboards()]);
        const configuredAndExisting = this.removeMissingDashboards(configured, remote);
        const allDashboards = (await this.addNewDashboards(configuredAndExisting, remote))
            // .map(dashboard => ({ id: `${dashboard.adapter}:${dashboard.remoteId}`, ...dashboard }));
            .map(dashboard => ({ ...dashboard }));
        const dashboardHash = allDashboards.reduce((hash, dash) => hash.set(getDashHashKey(dash.adapter, dash.remoteId), dash), new Map<string, Dashboard>());

        return [dashboardHash, await this.assignDashboardsToSections(sectionsHash, rootSections, allDashboards)];
    }

    private async loadConfiguredDashboards() : Promise<Dashboard[]> {
        return (await this.dataService
            .getDashboards())
            .map(stored => ({
                id: stored._id,
                adapter: stored.adapter,
                remoteId: stored.remoteId,
                title: stored.title,
                url: stored.url,
                section: stored.assignedTo
            }));
    }

    private async loadRemoteDashboards() {
        const responses = await Promise.all(
            this.adapters.map(
                adapter => adapter
                    .getDashboards()
                    .catch(e => (console.error(e), []))
            )
        );

        return responses.reduce((all, fromOne) => [...all, ...fromOne], []);
    }

    private removeMissingDashboards(configured: Dashboard[], remote: Dashboard[]) : Dashboard[] {
        return configured.filter(
            dashboard => !remote.every(
                remoteDashboard => remoteDashboard.adapter !== dashboard.adapter
                    || remoteDashboard.remoteId !== dashboard.remoteId
            )
        );
    }

    private async addNewDashboards(configured: Dashboard[], remote: Dashboard[]) : Promise<Dashboard[]> {
        const unknownDashboards = remote.filter(
            remoteDashboard => configured.every(
                dashboard => remoteDashboard.adapter !== dashboard.adapter
                    || remoteDashboard.remoteId !== dashboard.remoteId
            )
        );

        const newDashboards = (await Promise.all(unknownDashboards
            .map(unknown => this.dataService.setDashboardConfig(unknown))))
            .map(storedDashToDashboard);

        return [...configured, ...newDashboards];
    }

    private async assignDashboardsToSections(sectionsHash: Map<string, Section>, rootSections: Section[], dashboards: Dashboard[]) : Promise<Section[]> {
        await Promise.all(dashboards.map(async dashboard => {
            const section = await this.getSectionForDashboard(dashboard, sectionsHash);
            section.indicators.push(dashboard);
        }));

        return rootSections;
    }

    private async getSectionForDashboard(dashboard: Dashboard, sectionsHash: Map<string, Section>) : Promise<Section> {
        const section = sectionsHash.get(dashboard.section);
        if (section) {
            return section;
        }

        dashboard.section = UNASIGNED_SECTION;
        await this.dataService.setDashboardConfig(dashboard);

        return sectionsHash.get(UNASIGNED_SECTION)!;
    }

    async setDashboardConfig(adapter: string, remoteId: string, config: Partial<Dashboard>) : Promise<Dashboard> {
        const { sectionsHash, dashboardHash } = await this.getSections();
        const hashKey = getDashHashKey(adapter, remoteId);
        const currentConfig = dashboardHash.get(hashKey);
        if (!currentConfig) {
            throw new Error(`Le tableau de bord ${remoteId} n'existe pas sur ${adapter}`);
        }

        if (config.section) {
            const newParent = sectionsHash.get(config.section);
            if (!newParent) {
                throw new Error(`Section parente (${config.section}) inconnue`);
            }
        }

        const section = config.section
            || ('section' in config
                ? ''
                : currentConfig.section
            );

        const updated = { ...currentConfig, ...config, section };
        const saved = await this.dataService.setDashboardConfig(updated);
        this.invalidateCache();

        return storedDashToDashboard(saved);
    }

    async setSectionConfig(name: string, section: Partial<Section>) : Promise<Section> {
        if (name !== section.name) {
            throw new Error(`Le nom de la section (${name}) n'est pas en accord avec sa configuration`);
        }

        const { sectionsHash: sections } = await this.getSections();
        const existing = sections.get(section.name) || { };
        const updated = { ...existing, ...section };
        if (!updated.id) {
            updated.id = uuid.v1();
        }

        if (!updated.subSections) {
            updated.subSections = [];
        }

        if (!updated.indicators) {
            updated.indicators = [];
        }

        if (!isSection(updated)) {
            throw new Error('Configuration de section incomplète');
        }

        if (updated.parent) {
            const parent = sections.get(updated.parent);
            if (!parent) {
                throw new Error(`Section parente (${updated.parent}) inconnue`);
            }
        }

        const result = storedSectionToSection(await this.dataService.setSectionConfig(updated));
        this.invalidateCache();

        return result;
    }

    async deleteSection(name: string) : Promise<void> {
        const { sectionsHash } = await this.getSections();

        const section = sectionsHash.get(name);
        if (!section) {
            throw new Error(`La section avec le nom « ${name} » n'existe pas`);
        }

        await this.doDeleteSection(section);
        this.invalidateCache();
    }

    async doDeleteSection(section: Section) : Promise<void> {
        await Promise.all(section.subSections.map(async subSection => this.doDeleteSection(subSection)));

        const unasignedSection = (await this.getSections()).sectionsHash.get(UNASIGNED_SECTION)!;
        await Promise.all(section.indicators.map(async dashboard => {
            dashboard.section = UNASIGNED_SECTION;
            await this.dataService.setDashboardConfig(dashboard);
            unasignedSection.indicators.push(dashboard);
        }));

        return this.dataService.deleteSection(section);
    }

    async getComments(adapter: string, remoteId: string) : Promise<DashboardComment[]> {
        const dataItems = await this.dataService.getComments(adapter, remoteId);

        return dataItems.map(omitAll(['_id', '_rev'])) as DashboardComment[];
    }

    async addComment(adapter: string, remoteId: string, comment: DashboardComment) : Promise<DashboardComment[]> {
        await this.dataService.addComment(adapter, remoteId, comment);

        return this.getComments(adapter, remoteId);
    }
}

function storedSectionToSection(stored: DataItem<StoredSectionConfiguration>) : Section {
    return {
        id: stored._id,
        name: stored.name,
        title: stored.title,
        weight: stored.weight,
        parent: stored.parent,
        subSections: [],
        indicators: []
    };
}

function storedDashToDashboard(stored: DataItem<StoredDashConfiguration>) : Dashboard {
    return {
        adapter: stored.adapter,
        remoteId: stored.remoteId,
        url: stored.url,
        title: stored.title,
        section: stored.assignedTo,
        id: stored._id
    };
}

function getDashHashKey(adapter: string, remoteId: string | number) : string {
    return `${adapter}/${remoteId}`;
}
