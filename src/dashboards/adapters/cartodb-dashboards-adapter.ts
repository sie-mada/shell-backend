import { parse as parseCookie, serialize as serializeCookie } from 'cookie';
import * as FormData from 'form-data';
import { inject, injectable } from 'inversify';
import { omitAll } from 'lodash/fp';
import fetch from 'node-fetch';
import { ConfigService } from '../../config';
import { CartoDbConfig } from '../../config/types';
import { INJECTABLES } from '../../injectables';
import { buildUrlWithParams } from '../../util/fetch-utils';
import { LimitedLifetimeValue } from '../../util/limited-lifetime-value';
import { RemoteDashboardAdapter, UNASIGNED_SECTION } from '../dashboard-service';
import { Dashboard } from '../types';

type GetFormHeaders = (userHeaders?: Record<string, string>) => Record<string, string>;

interface CartoVizReponse {
    visualizations: CartoVisualization[];
    total_entries: number;
    total_user_entries: number;
    total_locked: number;
    total_likes: number;
    total_shared: number;
}

interface CartoVisualization {
    id: string;
    name: string;
    display_name: string | null;
    map_id: string;
    active_layer_id: string | null;
    type: 'derived';
    tags: string[];
    description: string | null;
    privacy: 'LINK';
    created_at: string;
    updated_at: string;
    locked: boolean;
    source: any;
    title: string | null;
    license: string | null;
    attributions: string | null;
    kind: 'geom';
    external_source: any;
    url: string;
    version: number;
    prev_id: any;
    next_id: any;
    parent_id: any;
    transition_options: any;
    active_child: any;
    children: any[];
    likes: number;
    synchronization: any;
    uses_builder_features: boolean;
    liked: boolean;
    permissions: CartoVizPermissions;
    stats: Record<string, number>;
    auth_tokens: any[];
    table: any;
}

interface CartoVizPermissions {
    id: string;
    owner: CartoUser;
    entity: CartoPermissionsEntity;
    acl: any[];
    created_at: string;
    updated_at: string;
}

interface CartoPermissionsEntity {
    id: string;
    type: 'viz';
}

interface CartoUser {
    id: string;
    name: string | null;
    last_name: string | null;
    username: string;
    email: string;
    avater_url: string;
    website: string | null;
    description: string | null;
    location: any;
    twitter_username: string | null;
    disqus_username: string | null;
    available_for_hire: boolean;
    base_url: string;
    google_maps_query_string: string | null;
    quota_in_bytes: number;
    tyble_count: number;
    viewer: boolean;
    org_admin: boolean;
    public_visualization_count: number;
    all_visualization_count: number;
    org_user: boolean;
    remove_logo: boolean;
    db_size_in_bytes: number;
}

@injectable()
export class CartoDbDashboardAdapter implements RemoteDashboardAdapter {
    readonly name: string = 'cartodb';

    private readonly cartoRootUrl: string;
    private readonly identity = {
        username: 'dev',
        password: 'pass1234'
    };

    private readonly currentSession = new LimitedLifetimeValue<Promise<Record<string, string>>>({ lifetime: 'P1D', initial: this.login.bind(this) });

    constructor(@inject(INJECTABLES.Config) config: ConfigService) {
        const cartoConfig = config.getSection<CartoDbConfig>('cartodb');
        this.cartoRootUrl = cartoConfig.rootUrl;
        this.identity.username = cartoConfig.username;
        this.identity.password = cartoConfig.password;
    }

    async getDashboards() : Promise<Dashboard[]> {
        const session = await this.currentSession.value || { };
        const params = {
            page: 1,
            exclude_shared: false,
            per_page: 1000,
            shared: 'no',
            locked: false,
            only_liked: false,
            order: 'updated_at',
            types: 'derived',
            deepInsights: false
        };
        const url = buildUrlWithParams(`${this.cartoRootUrl}/user/${this.identity.username}/api/v1/viz`, params);
        const response = await fetch(
            url,
            {
                headers: this.getBaseHeaders(session)
            }
        );

        if (!response.ok) {
            throw new Error('Erreur inattendue pendant la lecture des cartes disponibles de CartoDB');
        }

        const responseData = await response.json() as CartoVizReponse;

        return responseData.visualizations.map(this.toDashboard.bind(this));
    }

    private async login() : Promise<Record<string, string>> {
        const loginPageResponse = await fetch(`${this.cartoRootUrl}/user/${this.identity.username}/login`);
        const loginPage = await loginPageResponse.textConverted();

        const session = parseCookie(loginPageResponse.headers.get('Set-Cookie') || '');
        if (!session || !session._cartodb_session) {
            throw new Error('Impossible de retrouver la session de CartoDB');
        }

        const tokenMatch = /<input\s[^>]*?name="authenticity_token"\s+value="([^"]+)"/.exec(loginPage);
        if (!tokenMatch) {
            throw new Error('Impossible de retrouver le jeton d\'authenticité de CartoDB');
        }

        const token = tokenMatch[1];

        const loginForm = new FormData();
        loginForm.append('utf8', '\u2713');
        loginForm.append('authenticity_token', token);
        loginForm.append('email', this.identity.username);
        loginForm.append('password', this.identity.password);

        const loginResponse = await fetch(
            `${this.cartoRootUrl}/user/${this.identity.username}/sessions/create`,
            {
                method: 'POST',
                body: loginForm,
                redirect: 'manual',
                headers: (loginForm.getHeaders as GetFormHeaders)(this.getBaseHeaders(session))
            }
        );

        // CartoDB redirects to user default page after successful login but replies directly with
        // login form (no redirect) after invalid credentials are entered
        if (loginResponse.status !== 302) {
            throw new Error('Configuration des information d\'identification pour CartoDB invalide');
        }

        const loggedInSessionCookies = (loginResponse.headers.get('set-cookie') || '')
            .split(', ')
            .reduce((acc, cookie) => ({ ...acc, ...parseCookie(cookie) }), { });

        return omitAll(['domain', 'expires', 'path'], loggedInSessionCookies);
    }

    private getBaseHeaders(session: Record<string, string>) {
        return {
            Cookie: Object.entries(session)
                .map(entry => serializeCookie(entry[0], entry[1]))
                .join(', ')
        };
    }

    private toDashboard(viz: CartoVisualization) : Dashboard {
        return {
            id: '',
            adapter: this.name,
            remoteId: viz.id,
            section: UNASIGNED_SECTION,
            title: viz.title || viz.display_name || viz.name,
            url: viz.url
        };
    }
}
