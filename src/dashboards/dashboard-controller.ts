import * as Router from '@koa/router';
import { inject, injectable } from 'inversify';
import { Middleware } from 'koa';
import { Controller } from '../app';
import { AuthService } from '../auth';
import { INJECTABLES } from '../injectables';
import { DashboardService } from './dashboard-service';

@injectable()
export class DashboardController implements Controller {

    private readonly authenticated: Middleware;
    private readonly dataAdmin: Middleware;

    constructor(
        @inject(INJECTABLES.Auth) auth: AuthService,
        @inject(INJECTABLES.Dashboard) private readonly service: DashboardService
    ) {
        this.authenticated = auth.middleware();
        this.dataAdmin = auth.middleware(user => (user.groups || []).indexOf('data_admins') >= 0);
    }

    registerRoutes(router: Router, middleware?: Middleware | Middleware[], prefix: string = '') {
        const middlewares = !middleware
            ? []
            : Array.isArray(middleware)
            ? middleware
            : [middleware];

        router.get(`${prefix}/dashboard`, ...middlewares, this.authenticated, this.getAllDashboards.bind(this));
        router.get(`${prefix}/dashboard/:adapter/:remoteId/comment`, ...middlewares, this.authenticated, this.getComments.bind(this));
        router.post(`${prefix}/dashboard/:adapter/:remoteId/comment`, ...middlewares, this.authenticated, this.addComment.bind(this));
        router.put(`${prefix}/dashboard/section/:section`, ...middlewares, this.dataAdmin, this.setSectionConfig.bind(this));
        router.put(`${prefix}/dashboard/:adapter/:remoteId`, ...middlewares, this.dataAdmin, this.setDashboardConfig.bind(this));
        router.delete(`${prefix}/dashboard/section/:section`, ...middlewares, this.dataAdmin, this.deleteSection.bind(this));
    }

    async getAllDashboards(ctx: Router.RouterContext) {
        try {
            ctx.body = (await this.service.getSections()).rootSections;
        } catch (e) {
            ctx.throw(e.message || e.toString());
        }
    }

    async setSectionConfig(ctx: Router.RouterContext) {
        try {
            ctx.body = await this.service.setSectionConfig(ctx.params.section, ctx.request.body);
        } catch (e) {
            ctx.throw(e.message || e.toString());
        }
    }

    async setDashboardConfig(ctx: Router.RouterContext) {
        try {
            ctx.body = await this.service.setDashboardConfig(ctx.params.adapter, ctx.params.remoteId, ctx.request.body);
        } catch (e) {
            ctx.throw(e.message || e.toString());
        }
    }

    async deleteSection(ctx: Router.RouterContext) {
        try {
            ctx.body = await this.service.deleteSection(ctx.params.section);
        } catch (e) {
            ctx.throw(e.message || e.toString());
        }
    }

    async getComments(ctx: Router.RouterContext) {
        try {
            ctx.body = await this.service.getComments(ctx.params.adapter, ctx.params.remoteId);
        } catch (e) {
            ctx.throw(e.message || e.toString());
        }
    }

    async addComment(ctx: Router.RouterContext) {
        try {
            ctx.body = await this.service.addComment(ctx.params.adapter, ctx.params.remoteId, ctx.request.body);
        } catch (e) {
            ctx.throw(e.message || e.toString());
        }
    }
}
