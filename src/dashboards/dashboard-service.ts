import { Dashboard, DashboardComment, Section } from './types';

export const UNASIGNED_SECTION = 'unassigned';

export interface RemoteDashboardAdapter {

    readonly name: string;

    getDashboards() : Promise<Dashboard[]>;
}

export interface SectionsData {
    rootSections: Section[];
    sectionsHash: Map<string, Section>;
    dashboardHash: Map<string, Dashboard>;
}

export interface DashboardService {
    getSections() : Promise<SectionsData>;
    setDashboardConfig(adapter: string, remoteId: string, config: Partial<Dashboard>) : Promise<Dashboard>;

    setSectionConfig(name: string, section: Section) : Promise<Section>;
    deleteSection(name: string) : Promise<void>;
    getComments(adapter: string, remoteId: string) : Promise<DashboardComment[]>;
    addComment(adapter: string, remoteId: string, comment: DashboardComment) : Promise<DashboardComment[]>;
}
