import { createLogger, format, transports } from 'winston';
import { Context } from 'koa';

const logger = createLogger({
    transports: [
        new transports.Console({
            level: 'debug',
            format: format.combine(
                format.colorize(),
                format.simple()
            )
        }),
        new transports.File({ 
            filename: './backend-errors.log' , 
            level: 'error',
            format: format.combine(
                format.timestamp(),
                format.align(),
                format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
            )
        }),
        new transports.File({
            filename: 'backend.log',
            level: 'debug',
            format: format.combine(
                format.timestamp(),
                format.align(),
                format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
            )
        })
    ],
    exitOnError: false
});
 
export { logger };

const loggingService = (loggerInstance : typeof logger) => {

    return async (ctx: Context, next: () => Promise<any>): Promise<void> => {

        const start = new Date().getTime();

        const msgIn = `<-- ${ctx.method} ${ctx.originalUrl}`;

        loggerInstance.log('info', msgIn);


        await next();

        let directionString : string = '-->';
        if(ctx.request) {
            directionString = '<--';
        }
        if(ctx.response) {
            directionString = '-->';
        }

        const ms = new Date().getTime() - start;

        let logLevel: string;
        if (ctx.status >= 500) {
            logLevel = 'error';
        } else if (ctx.status >= 400) {
            logLevel = 'warn';
        } else {
            logLevel = 'info';
        }



        const msg = `${directionString} ${ctx.method} ${ctx.originalUrl} ${ctx.status} ${ms}ms`;

        loggerInstance.log(logLevel, msg);
    };    
};

export { loggingService };
