import { URL } from 'url';

export function buildUrlWithParams(base: string, params: any) : string {
    const url = new URL(base);
    if (params && typeof params === 'object') {
        Object.entries(params)
            .forEach(entry => url.searchParams.append(entry[0], `${entry[1]}`));
    }

    return url.toString();
}
