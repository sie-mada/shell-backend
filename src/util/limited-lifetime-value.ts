import * as moment from 'moment';

export interface Factory<T> {
    () : T;
}

export interface Destructor<T> {
    (target: T) : void;
}

export interface LimitedLifetimeOptions<T> {
    lifetime: string | moment.Duration;
    initial?: T | Factory<T>;
    destructor?: Destructor<T>;
    unref?: boolean;
    autoTouch?: boolean;
}

export class LimitedLifetimeValue<T> {

    private readonly lifetime: moment.Duration;
    private readonly unref: boolean;
    private readonly autoTouch: boolean;
    private readonly factory?: Factory<T>;
    private readonly destructor?: Destructor<T>;
    private unrefListener?: NodeJS.Timeout;
    private currentValue?: T;
    private lastUpdate?: moment.Moment;

    constructor({ lifetime, initial, destructor, unref = false, autoTouch = false }: LimitedLifetimeOptions<T>) {
        this.lifetime = moment.duration(lifetime);
        this.unref = unref;
        this.autoTouch = autoTouch;
        this.destructor = destructor;

        if (typeof initial === 'function') {
            this.factory = initial as Factory<T>;
        } else if (typeof initial !== 'undefined') {
            this.currentValue = initial;
            this.touch();
        }
    }

    update(newValue: T) {
        this.currentValue = newValue;
        this.touch();
    }

    touch() {
        this.lastUpdate = moment();
        if (this.unref) {
            if (this.unrefListener) {
                clearTimeout(this.unrefListener);
                this.unrefListener = undefined;
            }

            this.unrefListener = global.setTimeout(
                () => {
                    if (this.destructor && this.currentValue) {
                        this.destructor(this.currentValue);
                    }

                    this.currentValue = this.unrefListener = undefined;
                },
                this.lifetime.asMilliseconds()
            );
        }
    }

    get value() {
        if (!this.lastUpdate
            || this.lastUpdate
                .add(this.lifetime)
                .isBefore(moment())
        ) {
            if (!this.factory) {
                throw new Error('Le valeur a expiré');
            }

            this.currentValue = this.factory();
            this.touch();
        } else if (this.autoTouch) {
            this.touch();
        }

        return this.currentValue;
    }
}
