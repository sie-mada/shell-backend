import * as Router from '@koa/router';
import * as HttpStatus from 'http-status-codes';
import { inject, injectable } from 'inversify';
import { Middleware, ParameterizedContext } from 'koa';
import { Controller } from '../app';
import { INJECTABLES } from '../injectables';
import { AuthService, LdapService, User } from './types';
import { logger } from '../util/logger-service';

@injectable()
export class UserController implements Controller {

    private readonly adminOnly: Middleware;
    private readonly selfOrAdminOnly: Middleware;

    constructor(
        @inject(INJECTABLES.Ldap) private readonly ldap: LdapService,
        @inject(INJECTABLES.Auth) authorize: AuthService
    ) {
        this.adminOnly = authorize.middleware(user => (user.groups || []).indexOf('user_admins') >= 0);
        this.selfOrAdminOnly = authorize.middleware((user, url) => (user.groups || []).includes('user_admins') || url.endsWith(`/${user.id}`));
    }

    registerRoutes(router: Router, middleware?: Middleware | Middleware[], prefix: string = '') {
        const middlewares = !middleware
            ? []
            : Array.isArray(middleware)
            ? middleware
            : [middleware];

        router.post(`${prefix}/login`, ...middlewares, this.loginRequest.bind(this));
        router.get(`${prefix}/user`, ...middlewares, this.adminOnly, this.getUsers.bind(this));
        router.post(`${prefix}/user`, ...middlewares, this.adminOnly, this.registration.bind(this));
        router.get(`${prefix}/user/:id`, ...middlewares, this.selfOrAdminOnly, this.getUser.bind(this));
        router.put(`${prefix}/user/:id`, ...middlewares, this.adminOnly, this.updateUser.bind(this));
        router.delete(`${prefix}/user/:id`, ...middlewares, this.adminOnly, this.deleteUser.bind(this));
        router.get(`${prefix}/group`, ...middlewares, this.adminOnly, this.getGroups.bind(this));
    }

    async loginRequest(ctx: ParameterizedContext) {
        if (!ctx.request.body && ctx.request.body.payload) {
            ctx.throw('Details d\'utilisateur incomplètes');
        }

        logger.info(`Login request for user ${ctx.request.body.payload.username}`);

        const user = await this.ldap.searchLdapUser(ctx.request.body.payload.username);
        try {
            logger.debug(`Possibly found user (${user && user.id})`);
            await this.ldap.verifyUser(user, ctx.request.body.payload.password);
            logger.info(`User ${user!.id} verified`);
            ctx.response.body = JSON.stringify({
                username: user!.id,
                groups: user!.groups
            });
            ctx.status = HttpStatus.OK;
        } catch (error) {
            ctx.throw(HttpStatus.UNAUTHORIZED, error.message);
        }
    }

    async updateUser(ctx: ParameterizedContext) {
        if ((ctx.request.body as Partial<User>).id && ctx.params.id !== (ctx.request.body as User).id) {
            ctx.throw(HttpStatus.BAD_REQUEST, 'Impossible de modifier le nom d\'utilisateur');
        }

        const userData = { ...(ctx.request.body as Partial<User> & { password?: string }), id: ctx.params.id };

        try {
            ctx.body = await this.ldap.updateUser(userData);
        } catch (err) {
            ctx.throw(err);
        }
    }

    async registration(ctx: ParameterizedContext) {
        let user = await this.ldap.registerNewUser({
            id: ctx.request.body.username,
            firstName: ctx.request.body.firstName,
            lastName: ctx.request.body.lastName,
            mail: ctx.request.body.mail,
            password: ctx.request.body.password,
            groups: [...ctx.request.body.groups]
        });
        if (!user) {
            ctx.throw(HttpStatus.BAD_REQUEST, 'Utilisateur existe déjà');
        } else {
            ctx.body = user;
        }
    }

    async getUser(ctx: ParameterizedContext) {
        try {
            const user = await this.ldap.searchLdapUser(ctx.params.id);
            if (!user) {
                ctx.status = HttpStatus.NOT_FOUND;
            } else {
                ctx.body = userToExternalForm(user);
            }
        } catch (err) {
            ctx.throw(err);
        }
    }

    async deleteUser(ctx: ParameterizedContext) {
        try {
            this.ldap.deleteUser(ctx.params.id);
            ctx.status = HttpStatus.OK;
        } catch (err) {
            ctx.throw(err);
        }
    }

    async getUsers(ctx: ParameterizedContext) {
        try {
            ctx.body = (await this.ldap.getUsers()).map(userToExternalForm);
        } catch (err) {
            ctx.throw(err);
        }
    }

    async getGroups(ctx: ParameterizedContext) {
        try {
            ctx.body = await this.ldap.getGroups();
        } catch (err) {
            ctx.throw(err);
        }
    }
}

function userToExternalForm(user: User) {
    return {
        username: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
        mail: user.mail,
        groups: user.groups
    };
}
