import * as crypt from '@idango/crypt3/sync';
import { randomBytes } from 'crypto';
import { inject, injectable } from 'inversify';
import * as Ldap from 'ldapjs';
import { difference } from 'lodash/fp';
import { promisify } from 'util';
import { ConfigService } from '../config';
import { LdapConfig } from '../config/types';
import { INJECTABLES } from '../injectables';
import { AuthenticationError } from './authentication-error';
import { isNewUser, LdapService, NewUser, User } from './types';

const randomBytesP = promisify(randomBytes);

@injectable()
export class LdapServiceImpl implements LdapService {

    private readonly config: LdapConfig;

    constructor(@inject(INJECTABLES.Config) config: ConfigService) {
        this.config = config.getSection<LdapConfig>('ldap');
    }

    async searchLdapUser(id: string) : Promise<User | null> {
        return this.search(id);
    }

    async verifyUser(user: User | null, password: string) : Promise<boolean> {
        if (!user) {
            throw new Error('Détails d\'utilisateur invalides');
        }

        return new Promise((resolve, reject) => {
            const ldapClient = this.getLdapClient();

            ldapClient
                .bind(
                    user.dn,
                    password,
                    async err => {
                        if (err && err.name === 'InvalidCredentialsError') {
                            reject(new AuthenticationError('Détails d\'utilisateur invalides'));
                        } else if (err) {
                            reject(err);
                        } else {
                            resolve(true);
                        }

                        ldapClient.destroy();
                    }
                );
        });
    }

    getUserGroups(user: string) : Promise<string[]>;
    getUserGroups(user: User) : Promise<User>;
    async getUserGroups(user: User | string) : Promise<User | string[]> {
        const userId = typeof user === 'string'
            ? user
            : user.id;

        const ldap = this.getLdapClient();
        this.bind(ldap);
        let timeout: NodeJS.Timeout | null = null;

        try {
            const result = await new Promise<string[]>((resolve, reject) => {
                timeout = global.setTimeout(
                    () => reject('Recherche LDAP non terminé en temps'),
                    +this.config.timeout
                );

                ldap.search(
                    `${this.config.groupsUnit},${this.config.baseDn}`,
                    {
                        scope: 'sub',
                        filter: `(uniqueMember=${this.getUserDn(userId)})`
                    },
                    (initError, emitter) => {
                        if (initError) {
                            return reject(initError);
                        }

                        const groups: string[] = [];

                        emitter.on('error', reject);
                        emitter.on('searchEntry', entry => {
                            Array.isArray(entry.object.cn)
                                ? groups.concat(entry.object.cn)
                                : groups.push(entry.object.cn);
                            });
                        emitter.on('end', () => resolve(groups));
                    }
                );
            });

            if (typeof user === 'string') {
                return result;
            }

            user.groups = result;

            return user;
        } finally {
            if (timeout) { clearTimeout(timeout); }

            ldap.destroy();
        }
    }

    async updateUser(user: Partial<User> & { id: string, password?: string }) : Promise<User> {

        const modifications: Ldap.Change[] = [];

        if (user.password) {
            modifications.push(new Ldap.Change({
                operation: 'replace',
                modification: { userPassword: await this.hashPassword(user.password) }
            }));
        }

        if ('firstName' in user) {
            modifications.push(new Ldap.Change({
                operation: 'replace',
                modification: { [this.config.firstName]: user.firstName }
            }));
        }

        if ('lastName' in user) {
            modifications.push(new Ldap.Change({
                operation: 'replace',
                modification: { [this.config.lastName]: user.lastName }
            }));
        }

        if ('mail' in user) {
            modifications.push(new Ldap.Change({
                operation: 'replace',
                modification: { [this.config.mail]: user.mail }
            }));
        }

        if (modifications.length) {
            const ldapClient = this.getLdapClient();
            await this.bind(ldapClient, this.config.bind, this.config.bindPw);

            await new Promise((resolve, reject) =>
                ldapClient.modify(this.getUserDn(user.id), modifications, err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(true);
                    }

                    ldapClient.destroy();
                })
            );
        }

        if (user.groups) {
            const currentGroups = await this.getUserGroups(user.id);
            const groupsForRemoval = difference(currentGroups, user.groups);
            const groupsToAdd = difference(user.groups, currentGroups);

            for (const group of groupsForRemoval) {
                await this.removeFromGroup(user.id, group);
            }

            for (const group of groupsToAdd) {
                await this.addToGroup(user.id, group);
            }
        }

        const modifiedUser = await this.search(user.id);
        if (!modifiedUser) {
            throw new Error('Erreur interne');
        }

        return modifiedUser;
    }

    private async removeFromGroup(user: string, group: string) {
        return this.modifyGroupMembership('delete', user, group);
    }

    private async addToGroup(user: string, group: string) {
        return this.modifyGroupMembership('add', user, group);
    }

    private async modifyGroupMembership(operation: 'delete' | 'add', user: string, group: string) : Promise<void> {
        const change = new Ldap.Change({
            operation,
            modification: {
                uniqueMember: this.getUserDn(user)
            }
        });

        const ldapClient = this.getLdapClient();
        await this.bind(ldapClient, this.config.bind, this.config.bindPw);

        await new Promise((resolve, reject) => {
            ldapClient.modify(
                this.getGroupDn(group),
                change,
                err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }

                    ldapClient.destroy();
                }
            );
        });
    }

    async registerNewUser(newUser: NewUser) : Promise<User | null> {
        if (!isNewUser(newUser)) {
            return null;
        }

        const firstname = newUser.firstName || '';
        const lastname = newUser.lastName || newUser.id;
        let change: any = {
            [this.config.id]: newUser.id,
            [this.config.firstName]: firstname,
            [this.config.lastName]: lastname,
            [this.config.mail]: newUser.mail,
            userPassword: await this.hashPassword(newUser.password),
            objectClass: [
                'inetOrgPerson'
            ],
            cn: `${firstname} ${lastname}`
        };

        const ldapClient = this.getLdapClient();
        await this.bind(ldapClient);

        try {
            await new Promise((resolve, reject) => {
                ldapClient
                    .add(
                        `uid=${newUser.id},${this.config.usersUnit},${this.config.baseDn}`,
                        change,
                        err => {
                            if (err) {
                                reject(err);
                            } else {
                                resolve();
                            }
                        }
                    );
            });

            if (newUser.groups) {
                for (const group of newUser.groups) {
                    await this.addToGroup(newUser.id, group);
                }
            }

            return this.search(newUser.id);
        } catch (err) {
            return null;
        } finally {
            ldapClient.destroy();
        }
    }

    private async hashPassword(password: string) {
        const salt = Buffer
            .from(await randomBytesP(12))
            .toString('base64')
            .replace(/\+/g, '.');
        const crypt3Salt = `$6$${salt}`;
        const pwStorageStr = crypt(password, crypt3Salt);

        return `{CRYPT}${pwStorageStr}`;
    }

    async deleteUser(id: string) : Promise<void> {
        const ldapClient = this .getLdapClient();
        this.bind(ldapClient);

        try {
            await new Promise((resolve, reject) => {
                ldapClient.del(this.getUserDn(id), err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            });
        } finally {
            ldapClient.destroy();
        }
    }

    private getLdapClient() : Ldap.Client {
        return Ldap.createClient({
            url: this.config.url
        });
    }

    private async bind(ldapClient: Ldap.Client, dn?: string, password?: string) {
        return new Promise((resolve, reject) =>
            ldapClient
                .bind(
                    dn || this.config.bind,
                    password || this.config.bindPw,
                    err => {
                        return err ? reject(err) : resolve();
                    }
                )
        );
    }

    private async search(id: string) : Promise<User | null> {

        if (id === 'admin') {
            return {
                id: 'admin',
                firstName: 'Admin',
                lastName: '',
                mail: `admin@${this.urlFromDn(this.config.bind)}`,
                dn: this.config.bind
            };
        }

        const ldapClient = this.getLdapClient();
        await this.bind(ldapClient);
        let timeout: NodeJS.Timeout | null = null;

        try {
            return await new Promise<User | null>((resolve, reject) => {
                // nothing found after timeout
                timeout = global.setTimeout(
                    () => {
                        console.warn('Hit timeout for LDAP search');
                        reject('Registre d\'utilisateurs hors ligne');
                    },
                    +this.config.timeout
                );

                const options: Ldap.SearchOptions = {
                    filter: `(&(objectClass=inetOrgPerson)(|(${this.config.id}=${id})(mail=${id})(uid=${id})))`,
                    scope: 'sub',
                    attributes: [this.config.id, this.config.firstName, this.config.lastName, this.config.mail, 'dn', 'memberOf']
                };

                ldapClient
                    .search(
                        `ou=People,${this.config.baseDn}`,
                        options,
                        (err, res) => {
                            let resolved = false;

                            if (err) {
                                return reject(err);
                            }

                            res.on('error', e => {
                                reject(e);
                                resolved = true;
                            });
                            res.on('searchEntry', entry => {
                                const rawGroups = Array.isArray(entry.object.memberOf)
                                    // ? entry.object.memberOf as string[]
                                    // : typeof entry.object.memberOf === 'string'
                                    // ? [entry.object.memberOf]
                                    // : [];
                                    ? entry.object.memberOf as string[]
                                    : [entry.object.memberOf];
                                const idvar: string = entry.object[this.config.id] as string;
                                resolve({
                                    id: idvar,
                                    firstName: entry.object[this.config.firstName] as string,
                                    lastName: entry.object[this.config.lastName] as string,
                                    mail: entry.object[this.config.mail] as string,
                                    dn: entry.object.dn,
                                    groups: rawGroups.map(dnToGroup)
                                });
                                resolved = true;
                            });
                            res.on('end', () => {
                                if (!resolved) {
                                    reject('Nom d\'utilisateur inconnu');
                                }
                            });
                        }
                    );
            });
        } finally {
            if (timeout) {
                clearTimeout(timeout);
            }

            ldapClient.destroy();
        }
    }

    async getUsers() : Promise<User[]> {
        const ldapClient = this.getLdapClient();
        await this.bind(ldapClient);
        let timeout: NodeJS.Timeout | null = null;

        try {
            return await new Promise<User[]>((resolve, reject) => {
                // nothing found after timeout
                timeout = global.setTimeout(
                    () => {
                        console.warn('Hit timeout for LDAP search');
                        reject('Registre d\'utilisateurs hors ligne');
                    },
                    +this.config.timeout
                );

                const options: Ldap.SearchOptions = {
                    filter: '(objectClass=inetOrgPerson)',
                    scope: 'sub',
                    attributes: [this.config.id, this.config.firstName, this.config.lastName, this.config.mail, 'dn', 'memberOf']
                };

                const users: User[] = [];

                ldapClient
                    .search(
                        `ou=People,${this.config.baseDn}`,
                        options,
                        (initErr, res) => {
                            if (initErr) {
                                return reject(initErr);
                            }

                            res.on('error', err => {
                                reject(err);
                            });
                            res.on('searchEntry', entry => {
                                const rawGroups = Array.isArray(entry.object.memberOf)
                                    // ? entry.object.memberOf as string[]
                                    // : typeof entry.object.memberOf === 'string'
                                    // ? [entry.object.memberOf]
                                    // : [];
                                    ? entry.object.memberOf as string[]
                                    : [entry.object.memberOf];

                                users.push({
                                    id: entry.object[this.config.id] as string,
                                    firstName: entry.object[this.config.firstName] as string,
                                    lastName: entry.object[this.config.lastName] as string,
                                    mail: entry.object[this.config.mail] as string,
                                    dn: entry.object.dn,
                                    groups: rawGroups.map(dnToGroup)
                                });
                            });
                            res.on('end', () => {
                                resolve(users);
                            });
                        }
                    );
            });
        } finally {
            if (timeout) {
                clearTimeout(timeout);
            }

            ldapClient.destroy();
        }
    }

    async getGroups() : Promise<string[]> {
        const ldapClient = this.getLdapClient();
        await this.bind(ldapClient);
        let timeout: NodeJS.Timeout | null = null;

        try {
            return await new Promise<string[]>((resolve, reject) => {
                // nothing found after timeout
                timeout = global.setTimeout(
                    () => {
                        console.warn('Hit timeout for LDAP search');
                        reject('Registre d\'utilisateurs hors ligne');
                    },
                    +this.config.timeout
                );

                const options: Ldap.SearchOptions = {
                    filter: '(objectClass=groupOfUniqueNames)',
                    scope: 'sub',
                    attributes: ['cn']
                };

                const groups: string[] = [];

                ldapClient
                    .search(
                        `ou=Groups,${this.config.baseDn}`,
                        options,
                        (initErr, res) => {
                            if (initErr) {
                                return reject(initErr);
                            }

                            res.on('error', err => {
                                reject(err);
                            });
                            res.on('searchEntry', entry => {
                                groups.push(entry.object.cn as string);
                            });
                            res.on('end', () => {
                                resolve(groups);
                            });
                        }
                    );
            });
        } finally {
            if (timeout) {
                clearTimeout(timeout);
            }

            ldapClient.destroy();
        }
    }

    private getUserDn(user: string) {
        return `${this.config.id}=${user},${this.config.usersUnit},${this.config.baseDn}`;
    }

    private getGroupDn(group: string) {
        return `cn=${group},${this.config.groupsUnit},${this.config.baseDn}`;
    }

    private urlFromDn(dn: string) {
        return dn.split(',')
            .filter(part => part.startsWith('dc='))
            .map(part => part.substr(3))
            .join('.');
    }

}

function dnToGroup(groupDn: string) {
    return /cn=([^,]+)/.exec(groupDn || 'cn=!invalid!')![1];
}
