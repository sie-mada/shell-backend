import { FORBIDDEN, UNAUTHORIZED } from 'http-status-codes';
import { inject, injectable } from 'inversify';
import * as Koa from 'koa';
import * as Passport from 'koa-passport';
import * as BasicAuth from 'passport-http';
import { INJECTABLES } from '../injectables';
import { AuthenticationError } from './authentication-error';
import { LdapServiceImpl } from './ldap-service-impl';
import { AuthorizationValidator, AuthService } from './types';

@injectable()
export class AuthServiceImpl implements AuthService {

    constructor(@inject(INJECTABLES.Ldap) private readonly ldap: LdapServiceImpl) {
    }

    addBasicAuth(app: Koa) {
        app.use(Passport.initialize());

        Passport.use(new BasicAuth.BasicStrategy(async (userId, password, done) => {
            try {
                let user = await this.ldap.searchLdapUser(userId);
                await this.ldap.verifyUser(user, password);
                done(null, user);
            } catch (e) {
                if (e instanceof AuthenticationError) {
                    done(null);
                } else {
                    done(e);
                }
            }
        }));

        Passport.serializeUser((user, done) => {
            done(null, user);
        });

        Passport.deserializeUser((user, done) => {
            done(null, user);
        });
    }

    middleware(authorizer?: AuthorizationValidator) : Koa.Middleware {
        return async function AuthorizeMiddleware(ctx: Koa.ParameterizedContext, next: () => Promise<any>) {
            await authorize(ctx, authorizer);

            return next();
        };
    }
}

const basicAuth = Passport.authenticate('basic');

async function authorize(ctx: Koa.ParameterizedContext, authorizer?: AuthorizationValidator) {
    await (basicAuth(ctx, async () => undefined) as Promise<any>);

    if (!ctx.isAuthenticated()) {
        ctx.throw(UNAUTHORIZED, 'Accès limité');
    }

    if (typeof authorizer === 'function') {
        if (!authorizer(ctx.state.user, ctx.url)) {
            ctx.throw(FORBIDDEN, 'Accès limité');
        }
    }
}
