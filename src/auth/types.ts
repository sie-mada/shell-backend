import * as Koa from 'koa';

export interface User {
    id: string;
    firstName?: string;
    lastName?: string;
    mail: string;
    dn: string;
    groups?: string[];
}

export interface NewUser {
    id: string;
    mail: string;
    password: string;
    firstName?: string;
    lastName?: string;
    groups?: string[];
}

export function isNewUser(candidate: any) : candidate is NewUser {
    return !!candidate
        && typeof candidate.id === 'string'
        && typeof candidate.mail === 'string'
        && typeof candidate.password === 'string'
        && (!('firstName' in candidate) || typeof candidate.firstName === 'string')
        && (!('lastName' in candidate) || typeof candidate.lastName === 'string');
}

export interface LdapService {
    searchLdapUser(id: string) : Promise<User | null>;
    verifyUser(user: User | null, password: string) : Promise<boolean>;
    updateUser(user: Partial<User> & { id: string, password?: string }) : Promise<User>;
    registerNewUser(newUser: NewUser) : Promise<User | null>;
    deleteUser(id: string) : Promise<void>;
    getUserGroups(user: string) : Promise<string[]>;
    getUserGroups(user: User) : Promise<User>;

    getUsers() : Promise<User[]>;
    getGroups() : Promise<string[]>;
}

export interface AuthorizationValidator {
    (user: User, url: string) : boolean;
}

export interface AuthService {
    addBasicAuth(app: Koa) : void;
    middleware(authorizer?: AuthorizationValidator) : Koa.Middleware;
}
