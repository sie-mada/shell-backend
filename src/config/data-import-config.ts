import { DataImportConfig } from './types';

export const dataImportConfig: DataImportConfig = {
    importersDir: '{{data-dir}}/importers/dist'
    // importersDir: '/home/jsp/mada/sie-mada/dev-branch/importateurs/dist'
};
