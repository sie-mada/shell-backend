import { LdapConfig } from './types';

export const ldapConfig: LdapConfig = {
    url: 'ldap://{{ldap-host}}',
    port: 389,
    bind: 'cn=admin,{{ldap-base-dn}}',
    bindPw: '{{ldap-admin-password}}',
    baseDn: '{{ldap-base-dn}}',
    usersUnit: 'ou=People',
    groupsUnit: 'ou=Groups',
    id: 'uid',
    firstName: 'givenName',
    lastName: 'sn',
    mail: 'mail',
    timeout: '{{ldap-timeout}}'
};
