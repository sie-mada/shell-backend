import { ConfigService } from './config-service';
import { ConfigServiceImpl } from './config-service-impl';

export { ConfigService, ConfigServiceImpl };
