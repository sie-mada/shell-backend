export interface ConfigService {
    getSection<T extends { }>(name: string) : T;
}
