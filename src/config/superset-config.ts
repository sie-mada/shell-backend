import { SupersetConfig } from './types';

export const supersetConfig: SupersetConfig = {
    rootUrl: '{{superset-host}}',
    publicUrl: '{{superset-public-host}}',
    username: '{{superset-user}}',
    password: '{{superset-pass}}'
};
