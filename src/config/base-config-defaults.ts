export const baseConfigDefaults = {
    port: 8888,
    ldapTimeout: 10000,
    // dataDir: '/Users/midokarlos/Desktop/Evola',
    dataDir: '/home/jsp/mada/sie-mada',
    rootHostName: 'sie-madagascar.info',
    rootProtocol: 'http',
    apiHostName: 'localhost:8888',
    ldapHost: 'sie-madagascar.info',
    ldapBaseDn: 'dc=sie-madagascar,dc=info',
    ldapAdminPassword: 't0pSecret!',
    cartoHost: 'http://carto.sie-madagascar.info',
    cartoUser: 'dev',
    cartoPass: 'pass1234',
    supersetHost: 'http://superset.sie-madagascar.info',
    supersetPublicHost: '{{superset-host}}',
    supersetUser: 'sieadm',
    supersetPass: 't0pSecret!',
    dbHost: 'localhost',
    dbPort: 5432,
    dbName: 'sie',
    dbUser: 'postgres',
    dbPass: 't0pSecret!',
    mariadbHost: 'mariadb'
};
