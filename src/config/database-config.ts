import { DatabaseConfig } from './types';

export const databaseConfig: DatabaseConfig = {
    postgresHost: '{{db-host}}',
    postgresPort: +'{{db-port}}',
    postgresDb: '{{db-name}}',
    postgresUser: '{{db-user}}',
    postgresPassword: '{{db-pass}}',
    mariadbHost: '{{mariadb-host}}'
};
