import { injectable } from 'inversify';
import { camelCase, kebabCase } from 'lodash/fp';
import { baseConfig } from './base-config';
import { ConfigService } from './config-service';

const configProxyHandler: ProxyHandler<any> = {
    get(target: any, key: string) {
        const rawValue = target[key];
        if (typeof rawValue !== 'string') {
            return rawValue;
        }

        let processedValue = rawValue;
        while (/\{\{.*\}\}/.test(processedValue)) {
            processedValue = processedValue.replace(
                /\{\{(.*?)\}\}/g,
                (_, varName) => baseConfig[camelCase(varName) as keyof typeof baseConfig] as string || ''
            );
        }

        return processedValue;
    }
};

@injectable()
export class ConfigServiceImpl implements ConfigService {
    getSection<T extends { }>(name: string) : T {
        const moduleName = kebabCase(`${name}Config`);
        const configModule = require(`./${moduleName}`);
        const keys = Object.keys(configModule);
        if (!keys.length) {
            throw new Error('Module de configuration vide');
        }

        const configObjectKey = keys.find((_, index, allKeys) => allKeys[index].endsWith('Config')) || keys[0];
        const configObject = configModule[configObjectKey] as T;

        return new Proxy(configObject, configProxyHandler);
    }

}
